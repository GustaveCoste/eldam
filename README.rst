ELDAM
=====

.. image:: files/icons/ELDAM.png

**ELDAM (ELsa DAta Manager)** is a program developed by Gustave Coste (`gustave.coste@inra.fr <mailto:gustave.coste@inrae.fr>`_) for Life Cycle Analysis data management and conversion based on `ELSA <http://www.elsa-lca.org/>`_'s Quality Management System.

It allows conversion from SimaPro .xlsx export files to Elda spreadsheet and from Elda spreadsheet to SimaPro import csv file.

It features an integrated GUI for graphical use and can also be used easily within a Python environment with its API.

.. code-block:: python

    from eldam.core.simapro import SimaProXlsxReader, SimaProCsvWriter
    from eldam.core.elda import Elda

    # SimaPro export file -> Elda spreadsheet
    process = SimaProXlsxReader('path/to/xlsx/file.xlsx').to_processes()[0]
    elda = Elda()
    elda.add_version(process)
    elda.save('path/to/elda.xlsm')

    # Elda spreadsheet -> SimaPro import file
    elda = Elda('path/to/elda.xlsm')
    process = elda.read_last_version().to_process()
    SimaProCsvWriter(process).to_csv('path/to/export.csv')
  

Features
--------

- Conversion from SimaPro .xlsx export file to Elda spreadsheet
- Conversion from Elda spreadsheet to SimaPro import csv file
- Comparison between SimaPro export file and Elda spreadsheet
- Elda files indexing
- Graphical user interface
- Process detailed view
- Process quality check

Installation and download
-------------------------
ELDAM executables can be downloaded at: `framagit.org/GustaveCoste/eldam/wikis/FAQ/download <https://framagit.org/GustaveCoste/eldam/wikis/FAQ/download>`_

To use ELDAM as a Python package, it can be installed using :code:`pip install eldam-lca`.

**Warning:** *ELDAM's conversion feature to/from SimaPro only work if SimaPro's language is set to english.*

Documentation
-------------

- Documentation: `eldam-lca.readthedocs.io <https://eldam-lca.readthedocs.io>`_

References
----------

.. image:: https://joss.theoj.org/papers/10.21105/joss.02765/status.svg
   :target: https://doi.org/10.21105/joss.02765

Contribute
----------

- Issue Tracker: `framagit.org/GustaveCoste/eldam/issues <https://framagit.org/GustaveCoste/eldam/issues>`_
- Source Code: `framagit.org/GustaveCoste/eldam <https://framagit.org/GustaveCoste/eldam>`_

Support
-------

If you are having issues, please contact `gustave.coste@inrae.fr <mailto:gustave.coste@inrae.fr>`_.

Funded by
---------

.. image:: ../../files/icons/eu.png
.. image:: ../../files/icons/eu_occitanie.png
.. image:: ../../files/icons/occitanie.png


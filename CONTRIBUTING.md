First of all, thank you for taking the time to contribute to this project!

## How to contribute ?

#### Reporting bugs
If you found a bug, you can create an issue on [ELDAM's issue tracker](https://framagit.org/GustaveCoste/eldam/issues) with the `Bug` tag.
Before creating a new issue, please ensure that there is no opened issue about the same bug.

In order to make the bug correction easier, please add any information that may be useful, as well as any file that may help to reproduce the bug. 
For example if ELDAM crashes converting a SimaPro import file to an Elda file, indicate what you were trying to do, add the stack trace shown by the error window and attach the SimaPro export you were trying to convert.

#### Suggesting Enhancements
Enhancement suggestions are also welcome.
If you have a suggestion, you can create an issue on [ELDAM's issue tracker](https://framagit.org/GustaveCoste/eldam/issues) with the `Feature` tag.
Please be as exhaustive as possible about *what* you would like ELDAM to be able to do as well as *how* you think it could be done.

#### Submitting a Pull Request
If you have a code contribution, you can submit a pull request that will be examined and integrated if it contains substantial improvements.

Before to submit you pull request, please ensure that it passes all tests and is written in [PEP 8](https://www.python.org/dev/peps/pep-0008/) compliant Python code.

##### Editing EldaTemplate.xlsm
Before any edition of the Elda file template, be sure to read the [corresponding documentation](https://eldam-lca.readthedocs.io/en/latest/elda_layout_modification.html). 

## Code of conduct

Please follow the [Contributor Covenant Code of Conduct](https://www.contributor-covenant.org/version/2/0/code_of_conduct/).

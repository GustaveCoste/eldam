from PyQt5 import QtWidgets

from eldam.gui.mainwindow import MainWindow
import eldam.gui.exception_hook

if __name__ == '__main__':
    app = QtWidgets.QApplication([])
    window = MainWindow()
    window.show()
    app.exec()

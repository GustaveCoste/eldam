"""
Script computing statistics about a python package such as number of comment lines etc...
Inspired from http://code.activestate.com/recipes/527746-line-of-code-counter/ and
https://stackoverflow.com/a/5765045/4827616

Gustave Coste   -   gustavecoste@gmail.com   -   October 2017
"""

import collections
import os
import ast


def analyze(packagedir, exclude_dirs=None, exclude_files=None):
    """
    Analyses python code contained in a package
    :param str packagedir: Path of the package
    :param list exclude_dirs: Names of the directories to exclude
    :param list exclude_files: Filenames to exclude
    :return dict: Number of occurence by categorie (python statement or type of line)
    """
    if exclude_files:
        exclude_files.append('code_stats.py')
    else:
        exclude_files = ['code_stats.py']

    statistics = collections.defaultdict(int)

    # Walking thru every directory of the package
    for (dirpath, dirnames, filenames) in os.walk(packagedir):

        # Exclude specified directories
        if exclude_dirs:
            dirnames[:] = [d for d in dirnames if d not in exclude_dirs]

        for filename in filenames:
            if not filename.endswith('.py') or filename in exclude_files:
                continue

            filename = os.path.join(dirpath, filename)

            # Getting statistics on code
            syntax_tree = ast.parse(open(filename).read(), filename)
            for node in ast.walk(syntax_tree):
                statistics[type(node)] += 1

            # Getting statistics on lines
            skip = False
            for line in open(filename).readlines():
                statistics['line'] += 1

                line = line.strip()
                if line:
                    if line.startswith('#'):
                        statistics['comment_line'] += 1
                        continue
                    if line.startswith('"""'):
                        skip = not skip
                        continue
                    if not skip:
                        statistics['code_line'] += 1
                    else:
                        statistics['comment_line'] += 1

    statistics['blank_line'] = statistics['line'] - statistics['comment_line'] - statistics['code_line']
    statistics['code_line_proportion'] = statistics['code_line'] / statistics['line']
    statistics['comment_line_proportion'] = statistics['comment_line'] / statistics['line']
    statistics['blank_line_proportion'] = statistics['blank_line'] / statistics['line']

    return statistics


# Put directories to exclude here
EXCLUDE_DIRS = ['git', '.idea', '__pycache__', 'build', 'data', 'doc', 'env', 'files', 'output', 'tests', 'env36']
# Put filnames to exclude here
EXCLUDE_FILES = ['parameters.py', 'settings.py', 'conftest.py', 'cx_setup.py']

if __name__ == '__main__':
    stats = analyze('.', EXCLUDE_DIRS, EXCLUDE_FILES)

    print("Total number of lines:", stats['line'])
    print("Number of code lines:", stats['code_line'])
    print("Number of comment lines:", stats['comment_line'])
    print("Number of blank lines:", stats['blank_line'])
    print("Code line proportion:{:.0%}".format(stats['code_line_proportion']))
    print("Comment line proportion:{:.0%}".format(stats['comment_line_proportion']))
    print("Blank line proportion:{:.0%}".format(stats['blank_line_proportion']))
    print("Number of def statements:", stats[ast.FunctionDef])
    print("Number of class statements:", stats[ast.ClassDef])

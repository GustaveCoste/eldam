""" Pytest configuration file """
import pytest

import tests.test_data

collect_ignore = ["cx_setup.py", "manual_testing.py", "tests/update_test_files.py", "test_data.py"]


@pytest.fixture(scope="class")
def process_a():
    return tests.test_data.process_a()


@pytest.fixture(scope="class")
def process_a_constants():
    return tests.test_data.process_a_constants()


@pytest.fixture(scope="class")
def process_b():
    return tests.test_data.process_b()


@pytest.fixture(scope="class")
def process_c():
    return tests.test_data.process_c()


@pytest.fixture(scope="class")
def process_d():
    return tests.test_data.process_d()

"""
Setup file for compiling with cx_freeze. For compiling for mac, must be run from Mac OS

For PC run:
> python cx_setup.py  build_exe

For Mac run:
> python cx_setup.py  build_dmg
or
> python cx_setup.py  build_mac

See Also:
    https://cx-freeze.readthedocs.io/en/latest/distutils.html
"""

import sys
import os

import openpyxl
from cx_Freeze import setup, Executable

from eldam.settings import ELDAM_VERSION, DEBUG, ELDA_TEMPLATE_VERSION
from eldam.core.parameters import ELDA_TEMPLATE_FILEPATH

build_exe_options = {"packages": ["eldam", "asyncio", "idna"],
                     # asyncio and idna modules are needed but cx_freeze doesn't import them well without specifying it.
                     "include_files": ["files", "libcrypto-1_1.dll", "libssl-1_1.dll"]}

base = None
if sys.platform == "win32":
    if DEBUG:
        base = None
        verification = ''
        while verification not in ('y', 'Y', 'n', 'N'):
            verification = input(
                "Debug is activated. ELDAM will not display exception message in case of error, do you want to continue?\ny/n\n")
        if verification in ('n', 'N'):
            sys.exit()

    elda_template_version = str(openpyxl.load_workbook(ELDA_TEMPLATE_FILEPATH)['Status']['B22'].value)
    if elda_template_version != ELDA_TEMPLATE_VERSION:
        print(
            "The global variable ELDA_TEMPLATE_VERSION does not correspond to the version specified in the Elda template."
            "\nUpdate ELDA_TEMPLATE_VERSION in eldam/settings.py before compiling")
        sys.exit()

    else:
        base = "Win32GUI"

setup(
    name="ELDAM",
    version=ELDAM_VERSION,
    description='Tool to convert SimaPro process .xlsx export in an ELSA formatted elda',
    executables=[Executable("ELDAM.py", base=base, icon="files/icons/elsa.ico")],
    package_dir={'': ''},
    options={"build_exe": build_exe_options},
)

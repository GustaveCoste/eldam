Elda spreadsheet
================

**ELDAM** is based on a specially designed spreadsheet called **Elda**
(ELsa DAta). It stores information on a process such as process name and
flows, as well as metadata such as author contact or process technology.

The **Elda** spreadsheet is a .xlsm based format developed for storing
and editing LCI processes data. It can be opened by **MS Excel** or
equivalent (**LibreOffice Calc** for example), even if it has been
designed in **MS Excel 2016** and might then be more functional under
this software.

The file is composed by a *Status* worksheet that indexes the versions
of the process and one worksheet per version. For more information about
the review procedure, see :ref:`Reviewing LCI datasets <Reviewing LCI datasets>`.

Status sheet
------------

This sheet summarizes the content of the **Elda** by indexing every
review versions and their principal information.

.. figure:: ../_static/status_sheet.png
   :alt: Status sheet

It also indicates the **Elda** template version, needed to ensure
backward compatibility with **ELDAM**.

Process sheets
--------------

Process versions sheets contains every information about a process and
its flows and parameters.

It is divided into 6 subsections:

-  Version information
-  Process metadata
-  Mass balance
-  Flows
-  Input parameters
-  Calculated parameters
-  Review data (which is present on several subsections)

.. note:: To avoid accidental modification, the process sheet is
      protected by Excel. This protection can sometimes prevent
      non-accidental modifications. If you know what you are doing, you
      can unprotect the sheet by right-clicking on it and selecting
      *Unprotect sheet*.

.. figure:: ../_static/process_sheet_with_zones.png
   :alt: Process sheet

Version information
+++++++++++++++++++

This section stores information about the version of the process such as
version number and date of the version. The state of the review is
calculated automatically from the review state of each data.

.. figure:: ../_static/version_informations.png
   :alt: Version informations

For more information, see :ref:`Elda versioning system <Versioning System>`.

Process metadata
++++++++++++++++

This section stores general information on the data such as the process
name the author name and contact or general comment on the process.

.. figure:: ../_static/process_metadata.png
   :alt: Process metadata

Mass balance
++++++++++++

This section is mostly a tool for calculating a simple mass balance
between input and output flows.

.. figure:: ../_static/mass_balance.png
   :alt: Mass balance

Flows
+++++

.. figure:: ../_static/flows.png
   :alt: Flows

This section stores the process flows data. According to the flow type,
some fields are not relevant and appear shaded or are mandatory and
appear with a red background if they are empty. For example, inputs from
technosphere should have a “Library” but cannot have a compartment.

The *Amount* field can be filled with values or formulas (in that case a
“=” sign must be inserted before the formula). The formula can use
parameters from the input and calculated parameters sections by using
their names directly. The *Formula* field shows the formula entered in
the *Amount* field and shall not be edited.

Due to SimaPro data model, processes cannot have mix
*Output/Technosphere/Product* and *Output/Technosphere/Waste treatment
product* or have multiple *Output/Technosphere/Waste treatment product*.
In that case, the corresponding flows types will be highlighted in red.

For more information about *Flow modification*, *Flow relevance* and
*Numeric value confidence*, see :ref:`See Characterizing datasets Quality<Characterizing datasets quality>`.

Input and calculated parameters
+++++++++++++++++++++++++++++++

As in **SimaPro**, **Elda** files can use parameters for formula
calculations. These parameters can be defined by a value (input
parameters) or by a formula (calculated parameters). A parameter can be
defined at three levels: process, project and database. Project and
database parameters can be used by other processes of the same
**SimaPro** project or database.

For calculated parameters, write the formula preceded by “=” in the
*Value* field. The calculated value will appear in the *Value* field and
the formula will appear in the *Formula* field. It is possible to use
parameters in the formula by using their names directly.

For display reasons, parameters are grouped in blocks. If a block is
full, a new block will appear on its right allowing the user to enter
more parameters.

.. figure:: ../_static/input_parameters.png
   :alt: Input parameters

.. figure:: ../_static/calculated_parameters.png
   :alt: Calculated parameters

Review data
+++++++++++

Each data of the **Elda** can be reviewed with the *Checked* field. For
easier keyboard manipulation, this field uses a numeric code (**0**:
Invalid, **1**: Needs changes, **2**: Valid). Comments for and from the
reviewer can be added in the designated fields.

.. figure:: ../_static/review_data.png
   :alt: Review data

For more information about the review procedure, see :ref:`Reviewing LCI datasets <Reviewing LCI datasets>`.

Changed data
++++++++++++

Changes from one version to another are highlighted in yellow to make
them easier to spot.

.. figure:: ../_static/value_change.png
   :alt: Value change

Reviewing LCI datasets
======================

Versioning System
-----------------

ELDAM has been designed with version tracking in mind, for tracking the process's history as well as process review. It uses a two numbers versioning system (V1.0, V2.3, V1.5, ...). The first number is the process major version number. It only changes on major modelling change in the process (change of hypothesis, used technology, ...) and is a breaking point with the previous versions. The second number, minor version number, tracks the different review versions of a process.

The following figure explains the review procedure:

.. mermaid::

    graph TD
        Author(Author)
        Reviewer(Reviewer)
        A(Process Creation) --"(m=1, n=0)"--> B["Vm.n"]
        B --> C(Review)
        C --"n = n+1"--> D{Review result}
        D --Valid--> E[Final version]
        D -.Minor corrections.-> F[Vm.n]
        D ==Major corrections==> G[Vm.n]
        F-.->H(Modification)
        H-."n = n+1".->E
        G==>H
        H=="n = n+1"==>C
        E --> I(Major change)
        I--"m = m+1"-->B

        style Author fill:#47C1BB,stroke:#169471,stroke-width:3px,font-weight:bold
        style A fill:#47C1BB,stroke:#169471,stroke-width:3px,font-weight:bold
        style H fill:#47C1BB,stroke:#169471,stroke-width:3px,font-weight:bold
        style I fill:#47C1BB,stroke:#169471,stroke-width:3px,font-weight:bold

        style D fill:#4fa9ff,stroke:#0078ea,stroke-width:3px,font-weight:bold

        style Reviewer fill:#F39F1E,stroke:#eb6540,stroke-width:3px,font-weight:bold
        style C fill:#F39F1E,stroke:#eb6540,stroke-width:3px,font-weight:bold

        style B fill:#e98098,stroke:#7d428d,stroke-width:3px,color:white
        style E fill:#e98098,stroke:#7d428d,stroke-width:3px,color:white
        style F fill:#e98098,stroke:#7d428d,stroke-width:3px,color:white
        style G fill:#e98098,stroke:#7d428d,stroke-width:3px,color:white

Review states
-------------

Each data of the Life Cycle Inventory (LCI) must be reviewed. For this purpose, they all have a review state that takes three possible values (*Invalid*, *Needs changes*, and *Valid*). For an easier edition of the Elda, each review state has a code (**0**, **1** and **2**).

**Invalid** means that the author must change the data and submit it back to the reviewer.

**Needs changes** means that the author must do minor changes to the data but does not need to submit it back to the reviewer. Once the changes made, he can directly pass the review state to *Valid*.

**Valid** means that the data has been approved by the reviewer.

Elda versions have review states too, that are set automatically. The first version of the Elda is **Under progress**. If a version has at least one *Invalid* data, its review state is **Major corrections**. If it has no *Invalid* data but at least one data that *Needs changes*, its review state is **Minor corrections**. Finally, if every data is *Valid*, the versions review state is **Reviewed and valid**.

Data review states
++++++++++++++++++

+---------------+------+------------------------------------------------------------------------------------+
| Review state  | Code | Meaning                                                                            |
+===============+======+====================================================================================+
| Invalid       | 0    | The author must change the data and submit it back to the reviewer.                |
+---------------+------+------------------------------------------------------------------------------------+
| Needs changes | 1    | The author must do minor changes to the data and change the review state to Valid. |
+---------------+------+------------------------------------------------------------------------------------+
| Valid         | 2    | The data has been approved by the reviewer.                                        |
+---------------+------+------------------------------------------------------------------------------------+

Version review states
+++++++++++++++++++++

+--------------------+------------------------------------------------------------------------+
| Review state       | Meaning                                                                |
+====================+========================================================================+
| Under progress     | First version.                                                         |
+--------------------+------------------------------------------------------------------------+
| Major corrections  | There is at least one *Invalid* data.                                  |
+--------------------+------------------------------------------------------------------------+
| Minor corrections  | There is no *Invalid* data but at least one data that *Needs changes*. |
+--------------------+------------------------------------------------------------------------+
| Reviewed and valid | Every data is *Valid*.                                                 |
+--------------------+------------------------------------------------------------------------+

Review comments
---------------

Each data also has different types of comment fields. First of all, the *Comment* field contains information that helps to understand the data itself and that are not related to the review. The *Comment for reviewer* field contains information that the author wants to communicate to the reviewer during the review process. The *Reviewer’s comment* field contains information by the reviewer, explaining what he changed in the case of a minor correction by himself or what he expects in the case of a modification that must be done by the author.

Review comments can be removed by the author (if they are not relevant anymore) when changing a data’s review state from *Needs changes* to *Valid*. They can similarly be removed by the reviewer when changing a data’s review state from *Invalid* to *Valid*.

Instructions for authors
------------------------

When creating the first version of a LCI, the author fills every mandatory fields and potentially adds comment for the reviewer (if he is uncertain about a value for example). Once the first version is complete he sends it to the reviewer with all the related documents (cited in the Data source field of the flows).

When receiving a reviewed version, the author creates a new minor version and corrects the data marked *Invalid* or *Needs changes*. For data that *Needs changes*, he sets the review state to *Valid* and deletes the review comments (from author and from reviewer) if they are not relevant anymore. If there is no more *Invalid* data, then the LCI is *Reviewed and valid*, else he sends it back to the reviewer for another review iteration.

To easily spot what data have been changed, he can look at the reviewer version in which every modified data is highlighted in yellow.

Instructions for reviewers
--------------------------

When receiving an Elda, the reviewer creates a new minor version and looks at every *Invalid* data (all data in the case of a first version). The reviewer does several verifications:

    • He checks that mandatory data are filled in;
    • He checks that non-mandatory data are left blank only if they are not necessary to understand the process or are unknown;
    • He checks the consistency of each data;
    • He looks for typing errors;
    • He checks that every data is sufficiently documented.

Once the data has been checked, he does one of the four actions:

    • Set the review state to *Valid* and remove the review comments if they are not relevant anymore;
    • Make a minor correction (eg. a typo) and set the review state to *Valid* indicating the modification done in the *Reviewer’s comment* field;
    • Set the review state to *Needs changes* and indicate in the *Reviewer’s comment* field what needs correction;
    • Set the review state to *Invalid* and indicate in the *Reviewer’s comment* field what needs correction.

Use case
--------

Below is a simple use case scenario to illustrate the review procedure.

    The original author makes the first version of a LCI (V1.0) and sends it to a colleague for review. The reviewer makes some minor corrections (typos for example) and points out some errors to be changed by the author (V1.1). The version status is *Major corrections*. The author receives this version and corrects the errors (V1.2). The reviewer validates every data in a final version (V1.3). After some research, the author finds that the technology used in his original process was outdated and decides to change it for a more modern one. He makes his changes to creates a new version (V2.0) and sends it to the reviewer. The reviewer points out minor mistakes that must be corrected by the author, he creates a new minor version (V2.1) with the status *Minor corrections*. The author creates a new minor version (V2.2) and corrects the mistakes. The new final version is now V2.2.

Files management
----------------

In order to avoid concurrent versions of Eldas and to make review procedure easier, it is advised to store Eldas in a shared directory along with all the corresponding documents.
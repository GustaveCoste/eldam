Documenting LCI datasets
========================

To be reusable and understandable by everyone, a Life Cycle Inventory (LCI) must be documented. Documenting a LCI means adding all the information needed to understand what process it describes and how it has been built. This information, called metadata, is not processed by LCA software but is essential to LCA practitioners.

General Metadata
----------------
The following metadata are supported by ELDAM:

Author
++++++

+--------------------------+----------------------------------------------------------------------+----------------+
| Name                     | Name of the author                                                   | Mandatory      |
+--------------------------+----------------------------------------------------------------------+----------------+
| Contact mail             | Author’s email                                                       | Mandatory      |
+--------------------------+----------------------------------------------------------------------+----------------+
| Long-term contact        | Email of a person related to the project and available on long term  | Recommended    |
|                          | (mostly useful for non-permanent authors such as interns).           |                |
+--------------------------+----------------------------------------------------------------------+----------------+


Unit Process
++++++++++++
+--------------------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------------+
| Name                     | Name of the process                                                                                                                                                                                 | Mandatory      |
+--------------------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------------+
| Synonym                  | Synonym of the process                                                                                                                                                                              | Optional       |
+--------------------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------------+
| Allocation type          | Type of allocation used (if one)                                                                                                                                                                    | Mandatory      |
|                          |                                                                                                                                                                                                     | (if concerned) |
+--------------------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------------+
| Step                     | **End-of-life treatment:** Energy recycling, Waste water treatment, Landfilling                                                                                                                     | Mandatory      |
|                          |                                                                                                                                                                                                     |                |
|                          | **Energy carriers and technologies:** Electricity, Heat and steam, Mechanical energy, Crude oil based fuels, Natural gas based fuels, Hard coal based fuels, Lignite based fuels                    |                |
|                          |                                                                                                                                                                                                     |                |
|                          | **Materials production:** Plastics, Metals, Other mineral materials, Wood, Organic chemicals, Inorganic chemical, Water                                                                             |                |
|                          |                                                                                                                                                                                                     |                |
|                          | **Systems:** Packaging, Construction                                                                                                                                                                |                |
|                          |                                                                                                                                                                                                     |                |
|                          | **Transport services:** Water, Air, Other transport by Rail or Road                                                                                                                                 |                |
|                          |                                                                                                                                                                                                     |                |
|                          | (from ELCD categories)                                                                                                                                                                              |                |
+--------------------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------------+
| Project                  | Name of the project this process belongs to                                                                                                                                                         | Optional       |
+--------------------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------------+
| Step in the project      |                                                                                                                                                                                                     | Optional       |
+--------------------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------------+
| Reference year or period | Year or period of validity for the dataset                                                                                                                                                          | Mandatory      |
+--------------------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------------+
| Dataset validity limit   | Date until this dataset should be considered as obsolete                                                                                                                                            | Optional       |
+--------------------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------------+
| Location of the process  | Geographic location of the process                                                                                                                                                                  | Mandatory      |
+--------------------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------------+
| Technology description   |                                                                                                                                                                                                     | Mandatory      |
+--------------------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------------+
| Technology scale         | **Unspecified**; **Unknown**; **Laboratory scale**; **Pilot scale**; **Industrial scale**; **Mixed scale**                                                                                          | Mandatory      |
+--------------------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------------+
| Technology level         | **New:** for a technology assumed to be on some aspects technically superior to modern technology, but not yet the most commonly installed.                                                         | Recommended    |
|                          |                                                                                                                                                                                                     |                |
|                          | **Modern:** for a technology currently used when installing new capacity, when investment is based on purely economic considerations (most competitive technology).                                 |                |
|                          |                                                                                                                                                                                                     |                |
|                          | **Current:** for a technology in between modern and old.                                                                                                                                            |                |
|                          |                                                                                                                                                                                                     |                |
|                          | **Old:** for a technology that is currently taken out of use, when decommissioning is based on purely economic considerations (least competitive technology).                                       |                |
|                          |                                                                                                                                                                                                     |                |
|                          | **Outdated:** for a technology no longer in use.                                                                                                                                                    |                |
|                          |                                                                                                                                                                                                     |                |
|                          | **Undefined:** Market activities don't have a technology level.                                                                                                                                     |                |
+--------------------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------------+
| General comment          | Free text for general information about the dataset.                                                                                                                                                | Optional       |
+--------------------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------------+

Mass balance
++++++++++++

+--------------------------+-------------------------------------------+----------------+
| Mass of the inputs       | Mass of the material inputs               | Mandatory      |
+--------------------------+-------------------------------------------+----------------+
| Mass of the outputs      | Mass of the material outputs              | Mandatory      |
+--------------------------+-------------------------------------------+----------------+
| Mass difference          | If not 0, must be explained in comment    |Mandatory       |
+--------------------------+-------------------------------------------+----------------+

Flows
+++++

+----------------------+-----------------------------------------------------------------------------------------------------------------------------+----------------+
| Type                 | Flow type (Input from technosphere, product, …)                                                                             | Mandatory      |
+----------------------+-----------------------------------------------------------------------------------------------------------------------------+----------------+
| Name                 | Name of the flow as in SimaPro                                                                                              | Mandatory      |
+----------------------+-----------------------------------------------------------------------------------------------------------------------------+----------------+
| Library              | Database from which the flow comes                                                                                          | Mandatory      |
+----------------------+-----------------------------------------------------------------------------------------------------------------------------+----------------+
| Compartment          | For elementary flows only                                                                                                   | Mandatory      |
|                      |                                                                                                                             | (if concerned) |
+----------------------+-----------------------------------------------------------------------------------------------------------------------------+----------------+
| Sub-compartment/     | Sub-compartment for elementary flows.                                                                                       | Recommended    |
| Waste type           | Waste type for product and waste treatment product flows.                                                                   |                |
+----------------------+-----------------------------------------------------------------------------------------------------------------------------+----------------+
| Unit                 |                                                                                                                             | Mandatory      |
+----------------------+-----------------------------------------------------------------------------------------------------------------------------+----------------+
| Amount               | Amount of the flow in value or Excel formula. Parameters can be used in formulas with their name directly (as in SimaPro).  | Mandatory      |
+----------------------+-----------------------------------------------------------------------------------------------------------------------------+----------------+
| Formula              | Calculated automatically from Amount field                                                                                  | Calculated     |
+----------------------+-----------------------------------------------------------------------------------------------------------------------------+----------------+
| Allocation %         | Allocation percentage for products. Set to 100% if one product only.                                                        | Mandatory      |
|                      |                                                                                                                             | (if concerned) |
+----------------------+-----------------------------------------------------------------------------------------------------------------------------+----------------+
| Category             | Category of the flow (as in SimaPro). Subcategories can be used with the following format:                                  | Recommended    |
|                      |                                                                                                                             |                |
|                      | Category1/Category2/Category3                                                                                               |                |
+----------------------+-----------------------------------------------------------------------------------------------------------------------------+----------------+
| Data source          | Source of the data. If it is a publication or a report, the document must be included in the Elda’s folder.                 | Recommended    |
+----------------------+-----------------------------------------------------------------------------------------------------------------------------+----------------+
| Comment              | General comment on the flow                                                                                                 | Recommended    |
+----------------------+-----------------------------------------------------------------------------------------------------------------------------+----------------+
| Uncertainty          | Uncertainty type (*Lognormal*, *Normal*, *Triangle* or *Uniform*)                                                           | Optional       |
+----------------------+-----------------------------------------------------------------------------------------------------------------------------+----------------+
| SD                   | Standard deviation (for *Lognormal* and *Normal*)                                                                           | Optional       |
+----------------------+-----------------------------------------------------------------------------------------------------------------------------+----------------+
| Min                  | Minimum value (for *Triangle* and *Uniform*)                                                                                | Optional       |
+----------------------+-----------------------------------------------------------------------------------------------------------------------------+----------------+
| Max                  | Maximum value (for *Triangle* and *Uniform*)                                                                                | Optional       |
+----------------------+-----------------------------------------------------------------------------------------------------------------------------+----------------+
| Modification code    | :ref:`See Characterizing datasets Quality<Characterizing datasets quality>`                                                 | Mandatory      |
|                      |                                                                                                                             | (if concerned) |
+----------------------+-----------------------------------------------------------------------------------------------------------------------------+----------------+
| Modification comment | :ref:`See Characterizing datasets Quality<Characterizing datasets quality>`                                                 | Mandatory      |
|                      |                                                                                                                             | (if concerned) |
+----------------------+-----------------------------------------------------------------------------------------------------------------------------+----------------+
| Relevance code       | :ref:`See Characterizing datasets Quality<Characterizing datasets quality>`                                                 | Mandatory      |
+----------------------+-----------------------------------------------------------------------------------------------------------------------------+----------------+
| Relevance comment    | :ref:`See Characterizing datasets Quality<Characterizing datasets quality>`                                                 | Mandatory      |
|                      |                                                                                                                             | (if concerned) |
+----------------------+-----------------------------------------------------------------------------------------------------------------------------+----------------+
| Confidence code      | :ref:`See Characterizing datasets Quality<Characterizing datasets quality>`                                                 | Mandatory      |
+----------------------+-----------------------------------------------------------------------------------------------------------------------------+----------------+
| Confidence comment   | :ref:`See Characterizing datasets Quality<Characterizing datasets quality>`                                                 | Mandatory      |
+----------------------+-----------------------------------------------------------------------------------------------------------------------------+----------------+

Input parameters
++++++++++++++++

+-----------------+-------------------------------------------------------------------+-------------+
| Name            | Name of the parameter (as used in formulas)                       | Mandatory   |
+-----------------+-------------------------------------------------------------------+-------------+
| Value           |                                                                   | Mandatory   |
+-----------------+-------------------------------------------------------------------+-------------+
| Comment         | General comment on the parameter                                  | Recommended |
+-----------------+-------------------------------------------------------------------+-------------+
| Uncertainty     | Uncertainty type (*Lognormal*, *Normal*, *Triangle* or *Uniform*) | Optional    |
+-----------------+-------------------------------------------------------------------+-------------+
| SD              | Standard deviation (for *Lognormal* and *Normal*)                 | Optional    |
+-----------------+-------------------------------------------------------------------+-------------+
| Min             | Minimum value (for *Triangle* and *Uniform*)                      | Optional    |
+-----------------+-------------------------------------------------------------------+-------------+
| Max             | Maximum value (for *Triangle* and *Uniform*)                      | Optional    |
+-----------------+-------------------------------------------------------------------+-------------+
| Parameter level | SimaPro parameter level (*Process*, *Project* or *Database*)      | Mandatory   |
+-----------------+-------------------------------------------------------------------+-------------+

Calculated parameters
+++++++++++++++++++++

+-----------------+---------------------------------------------------------------------------------------+-------------+
| Name            | Name of the parameter (as used in formulas)                                           | Mandatory   |
+-----------------+---------------------------------------------------------------------------------------+-------------+
| Value           | Value of the parameter in Excel formula. Excel will compute the value and display it. | Mandatory   |
+-----------------+---------------------------------------------------------------------------------------+-------------+
| Formula         | Calculated automatically from *Value* field                                           | Calculated  |
+-----------------+---------------------------------------------------------------------------------------+-------------+
| Comment         | General comment on the parameter                                                      | Recommended |
+-----------------+---------------------------------------------------------------------------------------+-------------+
| Parameter level | SimaPro parameter level (*Process*, *Project* or *Database*)                          | Mandatory   |
+-----------------+---------------------------------------------------------------------------------------+-------------+

Dataset
+++++++

+------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+-------------+
| Review state           | *Under progress*; *Major corrections*; *Minor corrections*; *Reviewed and valid*                                                                                                     | Calculated  |
|                        |                                                                                                                                                                                      |             |
|                        | Automatically calculated from review data (see Reviewing LCI datasets).                                                                                                              |             |
+------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+-------------+
| Dataset version        | See Versioning system                                                                                                                                                                | Calculated  |
+------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+-------------+
| Version creator        | Creator of this version (author or reviewer)                                                                                                                                         | Mandatory   |
+------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+-------------+
| Contact                | Contact of the creator of this version                                                                                                                                               | Mandatory   |
+------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+-------------+
| Date of review         | Date of edition of this version                                                                                                                                                      | Mandatory   |
+------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+-------------+
| Comment                | Comment about the reviewing process and/or this specific version                                                                                                                     | Recommended |
+------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+-------------+
| Inventory review state | Review state of the inventory itself. This field can be used to force the review state if every data is valid but the inventory itself needs corrections (missing flow for example). | Optional    |
+------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+-------------+

Characterizing datasets quality
-------------------------------

All LCIs do not have the same quality level. A LCI may be of poor quality for various reasons:

    • It is a draft;
    • It has been done in a limited time;
    • The data on the process is inexistent.

Any LCI can be reused, regardless of its quality, provided it is well characterized. The poor quality of a LCI may only concern some of its flows. In that case, the practitioner reusing the LCI can improve these flows if he has more knowledge of the subject or more accurate data for example.

To finely characterize the quality of an LCI flows, three attributes have been defined. For each flow, several codified values are possible. Depending on this value, a comment specific to this flow may be necessary.

Flow modification
+++++++++++++++++

This attribute only concerns inputs from technosphere. It describes whether the flow comes from a database and whether it has been modified or used as is.

+------+-----------------------------------------------------------------------+----------------------------------------------------------------------------------------------+
| Code | Meaning                                                               | Additional information to specify                                                            |
+======+=======================================================================+==============================================================================================+
| 0    | The flow comes from a database as is.                                 |                                                                                              |
+------+-----------------------------------------------------------------------+----------------------------------------------------------------------------------------------+
| 1    | The flow is a slight adaptation of a process from a database.         | What has been modified and why.                                                              |
+------+-----------------------------------------------------------------------+----------------------------------------------------------------------------------------------+
| 2    | The flow is a strongly modified version of a process from a database. | What has been modified and why.                                                              |
+------+-----------------------------------------------------------------------+----------------------------------------------------------------------------------------------+
| 3    | The flow is a custom flow developed for the project.                  | Brief description of the newly created process and reason why it was necessary to create it. |
+------+-----------------------------------------------------------------------+----------------------------------------------------------------------------------------------+


Flow relevance
++++++++++++++

This attribute characterizes whether the flow corresponds to the reality it is supposed to describe. It applies to every flow except products.

+------+-------------------------------------------------------------------------------------------------------+-------------------------------------------------------------------------------------------------+
| Code | Meaning                                                                                               | Additional information to specify                                                               |
+======+=======================================================================================================+=================================================================================================+
| A    |  The flow is adapted to the reality of the described process                                          |                                                                                                 |
+------+-------------------------------------------------------------------------------------------------------+-------------------------------------------------------------------------------------------------+
| B    | The flow does not perfectly fit the reality of the described process but is the best available proxy. | The reality that the flow is supposed to describe and why this flow has been chosen as a proxy. |
+------+-------------------------------------------------------------------------------------------------------+-------------------------------------------------------------------------------------------------+

Confidence on numeric value
+++++++++++++++++++++++++++

This attribute describes the confidence level of the author about the amount of the flow.

+------+--------------------------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------------+
| Code | Meaning                                                                                                                              | Additional information to specify                             |
+======+======================================================================================================================================+===============================================================+
| Y    | The numeric value of the amount of the flow is considered trustworthy.                                                               | Origin of the numeric value.                                  |
+------+--------------------------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------------+
| Z    | The numeric value of the amount of the flow is not completely accurate and contains a significant but unknown amount of uncertainty. | Origin of the numeric value.                                  |
|      |                                                                                                                                      | Why this value is inaccurate and why has it been used anyway. |
+------+--------------------------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------------+

Data entry on SimaPro
+++++++++++++++++++++

Flow quality data can be entered directly under **Simpro** in the
comment field by respecting simple formatting rules:

-  The first line of the comment contains the quality data **codes**;
-  The following lines contains the **code** and the **comment** on this
   metadata;
-  If the data doesn’t need a comment, the comment lines can be skipped;
-  A **blank line** must be inserted between the quality data and the
   rest of the comment.

Default format:
***************

::

   1AY:
   1: Comment on modification
   A: Comment on relevance
   Y: Comment on confidence

   Rest of the comment.

The default format is the format in which comment will be created when
exporting a process from an **Elda** back to **SimaPro**. Still,
multiple formats can be used when entering data on **SimaPro**.

Examples:
*********

::

   -1,A,Y:
   1: Comment on modification
   Y: Comment on confidence

   Comment on relevance wasn't needed so the line is skipped.

::

   (1+A+Y)
   - 1 Comment on modification
   - Y Comment on confidence

   This comment
   is on

   multiple lines.

::

   0AY
   Y Only a comment on confidence

::

Data sources
------------

As for any scientific work, LCI data sources must be referenced. The field Data source of each flow must contain a reference to the source of the data used. If it is a web page, the page URL can be enough if the author considers that the website will remain accessible on long term (ex: Wikipedia), otherwise a copy of the webpage must be included in the Elda folder. Any other type of document (article, report, spreadsheet, …) must be included in the folder.

For particularly complex LCI, data sources can be grouped in subfolders for easier access. In that case, the path of the document must be included in the Data source field.

**Elda content (simplified):**

+-------------------------+-------------+--------+------+--------------------------------------+
| Type                    | Name        | Amount | Unit | Data source                          |
+=========================+=============+========+======+======================================+
| Product                 | Bread       | 820    | g    |                                      |
+-------------------------+-------------+--------+------+--------------------------------------+
| Input from technosphere | Flour       | 500    | g    | Ingredients/bread_recipe.pdf         |
+-------------------------+-------------+--------+------+--------------------------------------+
| Input from technosphere | Yeast       | 10     | g    | Ingredients/bread_recipe.pdf         |
+-------------------------+-------------+--------+------+--------------------------------------+
| Input from technosphere | Salt        | 10     | g    | Ingredients/bread_recipe.pdf         |
+-------------------------+-------------+--------+------+--------------------------------------+
| Input from technosphere | Oven usage  | 30     | min  | howtocookmybred.org/cooking_time     |
+-------------------------+-------------+--------+------+--------------------------------------+
| Input from technosphere | Tap water   | 285    | ml   | Ingredients/bread_water_content.xlsx |
+-------------------------+-------------+--------+------+--------------------------------------+
| Input from technosphere | Electricity | 1.75   | kWh  | Equipements/oven_manual.pdf          |
+-------------------------+-------------+--------+------+--------------------------------------+
| Output/                 | Water       | 15     | g    | Ingredients/bread_water_content.xlsx |
| Emission                |             |        |      |                                      |
+-------------------------+-------------+--------+------+--------------------------------------+

**Folder organization:**

+--------+--------------+--------------------------+
| Bread/ | Ingredients/ | bread_recipe.pdf         |
|        |              +--------------------------+
|        |              | bread_water_content.xlsx |
|        +--------------+--------------------------+
|        | Equipments/  | oven_manual.pdf          |
|        +--------------+--------------------------+
|        | bread.xlsm   |                          |
+--------+--------------+--------------------------+

Using parameters
----------------

As in SimaPro, Elda files can use parameters for formula calculations. These parameters can be defined by a value (input parameters) or by a formula (calculated parameters). A parameter can be defined at three levels: *process*, *project* and *database*. Project and database parameters can be used by other processes of the same SimaPro project or database.

For calculated parameters, write the formula preceded by **=** in the *Value* field. The calculated value will appear in the *Value* field and the formula will appear in the *Formula* field. It is possible to use parameters in the formula by using their names directly.

For display reasons, parameters are grouped in blocks. If a block is full, a new block will appear on its right allowing the user to enter more parameters.

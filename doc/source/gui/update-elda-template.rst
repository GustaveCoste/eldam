Updating an Elda template
=========================

**Elda** files are based on an Excel spreadsheet template that handles
functionalities like conditional formatting or version creation macros.
Like **ELDAM** itself, this template is subject to updates to add new
functionalities or correct bugs. Its version can be found in the
*Status* sheet of **Eldas**.

.. figure:: ../_static/status_sheet_template_number.png
   :alt: Status sheet

**Eldas** are automatically updated to the latest template version when
you add them a new minor or major version from a SimaPro export.

It is also possible to update one or more Elda templates without adding
data to it by opening **ELDAM** and going to *Update Elda template*.

Generating an Elda index
========================

When working with multiple Eldas stored in several folders, it can be
useful to have an overview of the data they contain without having to
open them all. This can be done easily by generating an Elda index with
ELDAM.

-  To do this, close every Elda in the directory you want to index
-  Open **ELDAM** and go to *Generate Elda index*.

.. figure:: ../_static/eldam_elda_index.png
   :alt: ELDAM main screen

-  Choose the directory containing the Eldas
-  Choose where to store the Elda index
-  Click on *Generate Elda index*
-  The index file is a spreadsheet containing one row per Elda and
   summarized information such as process name, version and product(s).

.. figure:: ../_static/elda_index.png
   :alt: ELDAM main screen

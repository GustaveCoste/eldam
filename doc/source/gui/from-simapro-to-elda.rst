From SimaPro to Elda
====================

**Eldas** can be created from a blank template (**EldaTemplate.xlsm**)
or can be converted from a **SimaPro** .xlsx export with **ELDAM**:

-  In **SimaPro**, save your process and go to the LCA explorer

-  Select your process and go to *File/Export*

      .. note:: You can also export several processes at once

   .. figure:: ../_static/simapro_lca_explorer.png
      :alt: SimaPro LCA Explorer

-  Export the process with the following setup:

   .. figure:: ../_static/simapro_export_setup.png
      :alt: SimaPro LCA Explorer

   ..

      .. note:: It is also possible to check the *Convert expression to
          constants* option. If so, every formulas of the process will be
          replaced by their result and parameters will not be exported. To
          ensure that this have been done on purpose, **ELDAM** will warn
          the user when opening the file.

-  Once the file exported, open **ELDAM.exe**

   .. figure:: ../_static/eldam_main_screen.png
      :alt: ELDAM main scree

-  Go to *Convert SimaPro export (.xlsx) to Elda* and select the SimaPro
   export file you want to convert.

      .. note:: You can also open several files at once

   .. figure:: ../_static/eldam_simapro_to_elda.png
      :alt: ELDAM process viewer

-  You can view your process in a tree and check the data read
   by **ELDAM**

-  When clicking on *Quality check*, **ELDAM** shows the flows with
   missing quality data.

   .. figure:: ../_static/eldam_simapro_to_elda_quality_check.png
      :alt: ELDAM quality check

-  If an **Elda** with the same name already exists, you can chose
   between the following options:

   -  decide for each (if multiple files only)

   -  overwrite the file

   -  update the existing **Elda**\ ’s last version

   -  add a minor version to the existing **Elda**

   -  add a major version to the existing **Elda**

-  Once you have set the export path, click on *Convert* to run the
   conversion.

..

   .. note:: If you have opened several processes (SimaPro export with
       multiple processes or multiple SimaPro exports), you can choose a
       directory to save the files as well as a naming pattern. The file
       naming pattern can contain several keywords that will be replaced:

       -  every occurrence of **PROCESS** in the name pattern will be
          replaced with the process’s name or by **process** if the process
          has no name
       -  every occurrence of **PRODUCT** will be replaced by the product
          name or by **product** if the process has several products
       -  every occurrence of **TODAY** will be replaced by the present date
          with **DD-MM-YY** format
       -  every occurrence of **NOW** will be replaced by the present time
          with **HH-MM-SS** format.

   **See:** :ref:`Difference between process and product name <Difference between process and product name>`

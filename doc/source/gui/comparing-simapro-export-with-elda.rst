Comparing SimaPro export with Elda
==================================

In some situations, for example if you get back to a project after a
while, it is necessary to check that the version of a process contained
in an **Elda** is the same that the version in **SimaPro**. In order to
avoid strenuous manually check of every data, **ELDAM** can compare the
data contained in an **Elda** and a **SimaPro** .xlsx export.

-  In **SimaPro**, save your process and go to the LCA explorer

-  Select your process and go to *File/Export*

    .. note:: Export only one process

   .. figure:: ../_static/simapro_lca_explorer.png
      :alt: SimaPro LCA Explorer

-  Export the process with the following setup:

   .. figure:: ../_static/simapro_export_setup.png
      :alt: SimaPro LCA Explorer

-  Once the file exported, open **ELDAM.exe**

   .. figure:: ../_static/eldam_main_screen.png
      :alt: ELDAM main screen

-  Choose *Compare a SimaPro export (.xlsx) with an existing Elda*.

-  Select the **SimaPro** export to compare.

-  Select the **Elda** to compare.

      .. note:: **ELDAM** will compare the **SimaPro** export with the
      last version of the process in the **Elda**.

-  If no differences are found between the two processes, this message
   appears:

   .. figure:: ../_static/eldam_comparison_no_difference.png
      :alt: ELDAM no differences message

-  Otherwise, **ELDAM** will show a process viewer with only the data
   that differs between the two versions:

   .. figure:: ../_static/eldam_comparison_with_difference.png
      :alt: ELDAM difference viewer

-  You can then choose to:

   -  Overwrite the **Elda** with the data of the **SimaPro** export
      (**Warning**\ *:This will erase all versions of the process in
      the*\ **ELDA**)
   -  Update the **Elda**\ ’s last version
   -  Append a minor or major version to the **Elda**.

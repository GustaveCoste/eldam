Copying Elda metadata
=====================

If you are working on a set of processes related to the same project, it
is likely that they will share all or part of their metadata. In order
to avoid entering metadata for each of the Elda corresponding to these
processes, **ELDAM** can copy the metadata of an Elda to a set of other
Eldas.

-  To do this, open **ELDAM** and got to *Copy Elda metadata*.

.. figure:: ../_static/eldam_copy_metadata.png
   :alt: ELDAM main screen


-  Choose the Elda from which metadata will be copied.
-  Choose the Elda(s) to which metadata will be copied.
-  Choose which metadata to copy
-  Click on *Copy metadata*

From Elda to SimaPro
====================

Once you have made modifications on an **Elda**, you can import it back
to **SimaPro** following these steps:

-  Open **ELDAM.exe**

   .. figure:: ../_static/eldam_main_screen.png
      :alt: ELDAM main screen

-  Go to *Convert Elda to SimaPro import (.csv)* and select the **Elda**
   to convert

      .. note:: You can also open several files at once

-  You can view your process(es) in a tree and check the information
   read by **ELDAM**

   .. figure:: ../_static/eldam_elda_to_simapro.png
      :alt: ELDAM process viewer

-  By clicking on *Quality check*, **ELDAM** shows the flows with
   missing quality data.

   .. figure:: ../_static/eldam_elda_to_simapro_quality_check.png
      :alt: ELDAM quality check

-  After checking your data, click on *Convert* to export the process to
   .csv format

      .. note:: You can check the *Export only data used by SimaPro*
          option to prevent **ELDAM** to export data that are not handled by
          **SimaPro** and would be displayed encoded in the comments such as
          review or quality data. However, by doing so these data will be
          lost and cannot be recovered on the next conversion to **Elda**.

-  Once the file exported, open **SimaPro**.

-  Go to *Librairies* and ensure that all libraires used by the imported
   processes are checked.

-  Then go to *File/Import*.

-  Click on *Add* and choose the .csv file to import.

      .. note:: If import files were already present, it might be
        necessary to click on *Clear* before being able to add files.

-  Set the import settings as below:

   .. figure:: ../_static/simapro_import_setup.png
      :alt: SimaPro import setup

-  **SimaPro** displays a summary of imported elements

   .. figure:: ../_static/simapro_import_summary.png
      :alt: SimaPro import summary

-  Click *OK* and the process(es) and parameter(s) are imported.

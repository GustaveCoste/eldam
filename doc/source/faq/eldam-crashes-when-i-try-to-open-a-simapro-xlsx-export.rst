ELDAM crashes when I try to open a SimaPro .xlsx export
=======================================================

In order for **ELDAM** to be able to read the process information, the
export must respect the following parameters:

.. figure:: ../_static/simapro_export_setup.png
   :alt: SimaPro LCA Explorer

If exporting your process with this setup did not solve your problem,
you can `report a bug <https://framagit.org/GustaveCoste/eldam/-/blob/master/CONTRIBUTING.md#reporting-bugs>`__.

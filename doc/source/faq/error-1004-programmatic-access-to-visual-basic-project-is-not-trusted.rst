Run-time error 1004: Programmatic access to Visual Basic Project is not trusted
===============================================================================

Elda files use VBA macro for some tasks. On opening an Elda the
following error messages may appear:

.. figure:: ../_static/VBA_project_warning.png
   :alt: Status sheet

.. figure:: ../_static/Run-time_error_1004.png
   :alt: Status sheet

In that case, you need open the *Macro security* setting in Excel’s
*Developer* toolbar, check *Trust access to the VBA project object
model* and reopen the Elda.

.. note::
   If you do not see the *Developer* toolbar, right-click on the ribbon
   and choose *Customize the ribbon* to activate it.

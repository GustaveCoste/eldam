The products flow type is highlighted in red in the Elda
========================================================

Due to SimaPro data model, processes cannot have mix
*Output/Technosphere/Product* and *Output/Technosphere/Waste treatment
product* or have multiple *Output/Technosphere/Waste treatment product*.
In that case, the corresponding flows types will be highlighted in red.

.. figure:: ../_static/product_type_error.png
   :alt: Status sheet

If you try to save an Elda with these errors you will be displayed a
warning (but the Elda will be saved anyway).

Difference between process and product name
===========================================

The distinction between processes and products names is a frequent
source of confusion. Each process has a name and one or more products.
Each product also has a name. In practice, it is frequent to work on
single-product processes that are referred by their product name. This
is accentuated by SimaPro, which does not require processes to have a
name and indexes them with the name of their product.

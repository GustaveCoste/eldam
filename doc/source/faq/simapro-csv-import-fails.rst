SimaPro fails to import the .csv file
=====================================

There are multiple reasons why SimaPro can fail to import a *.csv* file. One of those reasons is that the *.xlsx* file used to create the Elda file was exported from a SimaPro with another language setting than **english**. In that case, when trying to import the *.csv* file, SimaPro will show a message like this one :

.. figure:: ../_static/unkn own_category.png
   :alt: SimaPro error message


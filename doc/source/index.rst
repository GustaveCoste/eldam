Welcome to ELDAM documentation!
===============================

- `Project repository <https://framagit.org/GustaveCoste/eldam>`_
- `Executable download <https://framagit.org/GustaveCoste/eldam/-/wikis/FAQ/download>`_

..  toctree::
    :maxdepth: 1
    :caption: LCI documentation and review

    documentation-and-review/documentation
    documentation-and-review/review
    documentation-and-review/elda-spreadsheet

..  toctree::
    :maxdepth: 1
    :caption: Graphical User Interface

    gui/from-simapro-to-elda
    gui/from-elda-to-simapro
    gui/comparing-simapro-export-with-elda
    gui/copy-elda-metadata
    gui/generate-elda-index
    gui/update-elda-template

..  toctree::
    :maxdepth: 1
    :caption: Python package

    python-package/lci_data_format
    python-package/reading_from_xlsx
    python-package/writing_to_elda
    python-package/reading_from_elda
    python-package/writing_to_csv
    python-package/updating_elda_template
    python-package/indexing_elda_files
    python-package/gui_design
    python-package/parameters_and_settings
    python-package/editing_elda_spreadsheet
    python-package/elda_layout_modification
    python-package/testing
    python-package/compiling

..  toctree::
    :maxdepth: 1
    :caption: FAQ

    faq/flow-type-highlighted-in-red
    faq/difference-between-process-and-product-name
    faq/error-1004-programmatic-access-to-visual-basic-project-is-not-trusted
    faq/eldam-crashes-when-i-try-to-open-a-simapro-xlsx-export
    faq/simapro-csv-import-fails



..  toctree::
    :maxdepth: 1
    :caption: Changelog

    changelog



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`

eldam package
=============

Subpackages
-----------

.. toctree::

    eldam.core
    eldam.utils
    eldam.gui

Submodules
----------

eldam\.settings module
----------------------

.. automodule:: eldam.settings
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: eldam
    :members:
    :undoc-members:
    :show-inheritance:

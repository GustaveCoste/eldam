Parameters and settings
=======================

ELDAM's code separates code logic and static values. Static values are split into three files:
    *   `/eldam/core/parameters.py` that stores values used in ELDAM core code such as cell coordinates, field names, ...
    *   `/eldam/gui_parameters.py` that stores values used in ELDAM GUI such as dialog messages,
    *   `/eldam/settings.py` that stores global values such as program name, contact and version.
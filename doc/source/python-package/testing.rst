Testing ELDAM
=============

ELDAM has a testing environment set up with `pytest <https://docs.pytest.org/en/stable/>`_. Some tests use reference data for comparison. These data are stored in `/tests/data/` for workbooks and in `/conftest.py` for Python objects. Objects defined in `/conftest.py` can be accessed within a test by simply calling it as argument of the test function. More information on this feature is available in `pytest documentation <https://docs.pytest.org/en/latest/fixture.html#sharing-test-data>`_.

Functions :func:`~eldam.utils.xls.compare_workbook`, :func:`~eldam.utils.lci_data.compare_processes`, :func:`~eldam.utils.lci_data.compare_flows`, :func:`~eldam.utils.lci_data.compare_input_parameters`, :func:`~eldam.utils.lci_data.compare_calculated_parameters` can be used to spot differences between objects.

For simple functions, tests are written in Python doctests. More advanced unit or integration tests are written in `/tests/`.

Test processes
--------------

Tests are run on files and Python objects corresponding to test processes. Each object is referenced by a letter (a, b, c, ...) so for example, *elda_b.xlsm* is the elda corresponding to the process B.

These processes are:
 - **Process A**: a simple process containing only information handled by SimaPro.
 - **Process B**: a version of process A containing also information handled by the elda only (quality and review data, data source, ...).
 - **Process C**: a modified version of process A containing different flows and parameters.

*elda_a* and *elda_b* are the Elda spreadsheets corresponding to the respective processes. *elda_r* is an Elda containing process A as V1.0 and process B with metadata review info as V1.1. *elda_c_minor* and *elda_c_major* are Elda spreadsheets containing the two versions of *elda_r* plus a version of process C respectively as V1.3 and V2.0.

Updating test files
-------------------

Testing can sometimes be a painful task if not automatised enough. For example, for a modification of the expected output (elda or csv), every test files must be updated accordingly. To make this operation easier, a test file update script has been developed. This script located in *tests/data/update_test_files.py* performs an update of every elda and csv test files and puts them in an */update* folder. After verification of the files, they can be placed in the main test data folder, erasing the current files.

.. warning::
   As this script uses Eldam's code, an error in the code will be inherited by the generated files and may not raise an error upon testing (testing a wrong output against a wrong reference file). **This is why a manual check should always be done after updating the files.**

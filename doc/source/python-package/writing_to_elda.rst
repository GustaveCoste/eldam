Writing data to an Elda spreadsheet
====================================

The :class:`~eldam.core.elda.Elda` class is used to handle an elda file. If no filepath is provided to the :class:`~eldam.core.elda.Elda` constructor, the object will be created from a blank elda template.

Writing process data in an elda can be done by appending a new version to it or by updating last existing version, respectively with :meth:`~eldam.core.elda.Elda.add_version` and :meth:`~eldam.core.elda.Elda.update_last_version`. When adding a new version to the elda, the program creates a new sheet in the xls file by copying the last one and removing unwanted data. When copying the sheet,  some style elements are lost, this can be fixed with :meth:`~eldam.core.elda.Elda.fix_style`. Similarly, the comments position and size are lost when reading the sheet. This is fixed by :meth:`~eldam.core.elda.Elda.fix_comment_size`.

According to the type of the new version (major or minor), more or less data will be removed. For major versions, every data is removed and the version is created from scratch. For minor versions, every data input by the user itself are kept, only data automatically generated on basis of the .xlsx content are removed.

.. seealso:: :ref:`Elda versioning system<Versioning system>`

The data removal and insertion is not made by the instance of :class:`~eldam.core.elda.Elda` itself but by an instance of :class:`~eldam.core.elda.EldaVersionWriter` created by :meth:`~eldam.core.elda.Elda.add_version` , :meth:`~eldam.core.elda.Elda.update_last_version` or :meth:`~eldam.core.elda.Elda.update_last_version_metadata`.

:class:`~eldam.core.elda.EldaVersionWriter` is the class used to write data in an elda sheet. It has methods for removing and inserting data (:meth:`~eldam.core.elda.EldaVersionWriter.clean_data` and :meth:`~eldam.core.elda.EldaVersionWriter.add_process_data`). Its method :meth:`~eldam.core.elda.EldaVersionWriter.add_process_metadata` only populates the metadata fields (except the name and synonym). It is used by :meth:`~eldam.core.elda.Elda.update_last_version_metadata` to copy metadata from an Elda to another.

:class:`~eldam.core.elda.EldaVersionWriter` also features a method named :meth:`~eldam.core.elda.EldaVersionWriter.insert_parameter` that is called by :meth:`~eldam.core.elda.EldaVersionWriter.add_process_data` and allows to insert an :class:`~eldam.core.lci_data.InputParameter` or a :class:`~eldam.core.lci_data.CalculatedParameter` data and to add a named cell referring to this parameter value cell in the workbook. When a parameter block is full, :meth:`~eldam.core.elda.EldaVersionWriter.insert_parameter` will call :meth:`~eldam.core.elda.EldaVersionWriter.add_parameter_block` and generate a new parameter block.

After adding a new version or updating last version of an elda, data must be saved with :meth:`~eldam.core.elda.Elda.save`.

.. code-block:: python

 from eldam.core.elda import Elda

 # Creating an elda from scratch
 new_elda = Elda()
 new_elda.add_version(process)
 new_elda.save('path/to/elda')

 # Updating an existing elda
 elda = Elda('path/to/elda')
 elda.update_last_version(process_2)
 elda.add_version(process3, major_version=True)
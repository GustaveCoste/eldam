Compiling ELDAM
===============

To be used outside of a python environment, ELDAM can be compiled in a .exe file bundled with a Python interpreter and all needed dependencies. This is done with the `cx_Freeze <https://cx-freeze.readthedocs.io/>`_ package. Compiling can be launched with `/compile.bat`.

.. warning::
    Compiling on a 64/32bit machine will produce an executable for 64/32bit machines only.
.. _elda-layout-modification-ref:

Elda layout modification
------------------------

Several pieces of ELDAM's code interact with the content of the Elda files. These codes can be located in the Elda file VBA macros or in the Python code. To ensure that they know where to read or write data, some cell and range addresses are hard coded. When changing the Elda template layout, these addresses must be updated. For example, if adding a flow field and thus a column, all addresses of cells located right of this column must be moved by one column.

The following procedure ensures that the modifications will be seamlessly propagated.

- Open EldaTemplate.xlsm
- Disable protection on *Status* worksheet (password is **eldam**).
- Increment elda template version number
- If relevant, update the formulas displaying the version information
- Enable protection on *Status* worksheet
- If relevant, update the value of *input_parameter_last_cell* and *calculated_parameter_last_cell* (right of the version info box)
- Open VBA editor and update every hardcoded cell or range addresses
- If the parameters blocks dimensions have been changed update the *Worksheet_Change* macro
- Also update the macro attributing defined names to parameters
- Check protection is enabled on every sheet of the Elda template
- Save and close the Elda template
- Add a new *if* block to the :meth:`~eldam.core.parameters.EldaTemplateParameters.load_version_parameters` method corresponding to the previous elda template version
- For each modified hardcoded cell or range address as well as *CHANGED_VALUE_CF_FORMULA*:

    - Copy the previous value to the *if* block newly created
    - Update its value in the constructor of :class:`~eldam.core.parameters.EldaTemplateParameters`
- If the parameters blocks dimensions have been changed do the same for *INPUT_PARAMETERS_BLOCK_HEIGHT*, *INPUT_PARAMETERS_BLOCK_WIDTH*, *CALCULATED_PARAMETERS_BLOCK_HEIGHT*, *CALCULATED_PARAMETERS_BLOCK_WIDTH*, *PARAMETERS_FIRST_ROW* and *PARAMETERS_LAST_ROW*
- Update the template version number in **eldam.settings.py**
- Update test files
- Manually check updated test files
- Run tests
- Commit
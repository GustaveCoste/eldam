Reading from a SimaPro .xlsx export file
========================================

Exporting a SimaPro process
---------------------------

SimaPro features multiples ways and format to export processes. ELDAM has been designed to handle only .xlsx files. They must be exported via SimaPro's main LCA Explorer view (select a process and then `File/Export`).

Export parameters must be set as shown in the image below:

.. image:: ../_static/export_setup.PNG
    :name: Export setup
    :target: ../_static/export_setup.PNG

Reading a SimaPro .xlsx file
----------------------------

The :class:`~eldam.core.simapro.SimaProXlsxReader` class allows to read and extract data from a SimaPro .xlsx export file. A :class:`~eldam.core.simapro.SimaProXlsxReader` object is built from a SimaPro export file path and contains a list named :attr:`~eldam.core.simapro.SimaProXlsxReader.processes_data` with every data collected from the SimaPro export for each process and a list named :attr:`~eldam.core.simapro.SimaProXlsxReader.common_parameters_data` containing the project and database level parameters exported.

The boolean attribute :attr:`~eldam.core.simapro.SimaProXlsxReader.convert_expressions_to_constants` indicates if the processes have been exported from SimaPro with the *Convert expressions to constants* option.

The method :meth:`~eldam.core.simapro.SimaProXlsxReader.to_processes` allows to convert the SimaPro export to a list of instances of :class:`~eldam.core.lci_data.Process`.

.. code-block:: python

 from eldam.core.simapro import SimaProXlsxReader

 filepath = 'path/to/export/file.xlsx'
 processes = SimaProXlsxReader(filepath).to_processes()
 process = processes[0]

Updating elda template
======================

Elda files are based on a template that is subject to improvement and modifications. The template version is stored on the elda *Status* sheet. The current elda template is also stored in :const:`~eldam.settings.ELDA_TEMPLATE_VERSION`. When reading an elda, the :class:`~eldam.core.elda.EldaVersionReader` will compare the template version of the file to the current template version. In case of difference, the elda file will automatically be updated by :meth:`~eldam.core.elda.Elda.update_elda_template`.
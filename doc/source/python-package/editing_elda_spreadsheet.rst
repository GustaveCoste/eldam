Editing the Elda spreadsheet
============================

The **Elda** (ELsa DAta) spreadsheet is a .xlsm based format developed for storing and editing LCI processes data. It can be opened by MS Excel or equivalent (LibreOffice Calc for example), even if it has been designed in MS Excel 2016 and might then be more functional under this software.

The file is composed of a *Status* worksheet and one worksheet per version of the process.

Elda spreadsheet structure
--------------------------

.. seealso:: For information on the Elda spreadsheet structure, :ref:`see Elda spreadsheet <Elda spreadsheet>`.

.. note:: To prevent from user modification, the *Status* sheet is protected by a password.

 The password is: **eldam**

.. seealso:: When modifying the structure of the Elda spreadsheet template (ex: adding a flow field), certain changes must be made to the VBA macros or to the Python code. The procedure is detailed in :ref:`elda-layout-modification-ref`.



Styling
-------

Except from the macros triggered by the *Status* sheet buttons, every dynamic in the Elda spreadsheet is done using conditional formatting:

.. image:: ../_static/conditional_formatting.PNG
    :name: Conditional formatting
    :target: ../_static/conditional_formatting.PNG

Required fields
...............

Some required fields will have a light-red background unless they are filled. This is done with a conditional formatting that will trigger if the cell is empty and a reference cell is filled or changed from its default value.

Blocked fields
..............

Blocked fields work exactly as required fields with a different formatting.

Flow type, review and quality data font color
.............................................

To make reading easier and avoid errors, some fields are colored according to their content or the content of another cell.

Changed values
..............

Changes from one version to another are highlighted in yellow to make them easier to spot. This is done by a conditional formatting comparing the cell content to the content of the same cell in the previous sheet.

VBA code
--------

Some actions within the Elda are done with VBA code and macros. Due to the low level of accessibility and comfort of the VBA language, most things have been done within ELDAM's code in Python. Still, some features concerning the Elda's usage had to be coded in VBA.

Version related macros
......................
VBA macros are used to generate new minor or major versions from the **Status** worksheet. **AddNewMinorVersion()** and **AddNewMajorVersion()** are triggered by their respective buttons. **UpdateVersions()** updates the versions list in the status worksheet. It is triggered by its own button but also by the two version creation macros and at the opening of the file. **CleanReviewData()** is called by **AddNewMajorVersion()** for removing review data of the newly created worksheet.

Parameters block macro
......................
The apparition of a new parameter block when the previous is full is done by a VBA macro on the worksheet's code (to display it, right-click on the worksheet and select *View code*). The macro looks at modification on any cell. If the modified cell is the last name cell of a parameter block (whose coordinates are stored in cells **Q1** and **Q2**) and the next block haven't already been created, then the macro copies a new block next to the existing one.

When generating a new worksheet (version) with ELDAM, the worksheet's code is lost. To fix this, a macro on the workbook itself has been coded that copies code from the second worksheet's code (V1.0) to the following worksheets when opening the file. This macro needs access to the **VBA project object model** and will display an error on opening the workbook unless the user have enabled access to the **VBA project object model**.

.. seealso:: For more information on the access to the **VBA project object model**, see :ref:`FAQ <Run-time error 1004: Programmatic access to Visual Basic Project is not trusted>`.
Reading data from a Elda spreadsheet
====================================

Reading data from an elda spreadsheet is quite similar to reading from a SimaPro .xlsx export file. It is made via the :class:`~eldam.core.elda.EldaVersionReader` class and its method :meth:`~eldam.core.elda.EldaVersionReader.to_process`. It is also possible to do it via the :class:`~eldam.core.elda.Elda` class and its methods :meth:`~eldam.core.elda.Elda.read_last_version` and :meth:`~eldam.core.elda.Elda.read_all_versions_to_processes`.

The constructor of :class:`~eldam.core.elda.EldaVersionReader` will look at the elda template version number and use the parameters specific to this version with :class:`~eldam.core.parameters.EldaTemplateParameters`. This allows to read Elda built with previous versions of the Elda template. :class:`~eldam.core.parameters.EldaTemplateParameters` contains all the Elda template version specific parameters such as cells and ranges addresses. The default values correspond to the current Elda template and can be replaced by a specific Elda template version with method :meth:`~eldam.core.parameters.EldaTemplateParameters.load_version_parameters`.

.. code-block:: python

 from eldam.core.elda import Elda, EldaVersionReader

 # Reading a specific version of an elda
 elda = Elda('path/to/elda.xlsm')
 reader = EldaVersionReader(elda, 'V1.3')
 process = reader.to_process()

 # Reading last version of an elda
 elda = Elda('path/to/elda.xlsm')
 process = elda.read_last_version().to_process()
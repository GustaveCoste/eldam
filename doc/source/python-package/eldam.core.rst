eldam\.core package
===================

Submodules
----------

eldam\.core\.elda module
------------------------

.. automodule:: eldam.core.elda
    :members:
    :undoc-members:
    :show-inheritance:

eldam\.core\.elda module
------------------------

.. automodule:: eldam.core.elda_index
    :members:
    :undoc-members:
    :show-inheritance:

eldam\.core\.lci\_data module
-----------------------------

.. automodule:: eldam.core.lci_data
    :members:
    :undoc-members:
    :show-inheritance:

eldam\.core\.parameters module
------------------------------

.. automodule:: eldam.core.parameters
    :members:
    :undoc-members:
    :show-inheritance:

eldam\.core\.simapro module
---------------------------

.. automodule:: eldam.core.simapro
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: eldam.core
    :members:
    :undoc-members:
    :show-inheritance:

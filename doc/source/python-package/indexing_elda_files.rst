Indexing Elda files
===================

To make Elda files management easier, ELDAM provides an Elda files indexing feature.

Elda files indexing is done by the :class:`~eldam.core.elda_index.EldaIndexer` class. Its method :meth:`~eldam.core.elda_index.EldaIndexer.build_index` will scan a directory and try to read each **.xlsm** files as Eldas to gather information on them. :meth:`~eldam.core.elda_index.EldaIndexer.save_index` will export the index in **.xlsx** format.

.. code-block:: python

 from eldam.core.elda_index import EldaIndexer

 elda_indexer = EldaIndexer('/path/of/the/directory')
 elda_indexer.build_index()
 elda_indexer.save_index()

.. Note::
    As indexing a large number of Elda files can take a long time, the :class:`~eldam.core.elda_index.EldaIndexer` class uses the `observer design pattern`__ to notify any potential suscriber of its progress. This is used for updating a progressbar in the dedicated GUI widget.

.. __: https://en.wikipedia.org/wiki/Observer_pattern
Life Cycle Inventory data format
================================

The main goal of ELDAM is Life Cycle Inventory data conversion. This is done in an object oriented way using LCI abstract classes that simply describes LCI data without any file format considerations. These classes are an intermediate state allowing easier management and interoperability.

Process
-------

The main data structure is the :class:`~eldam.core.lci_data.Process` class. It represents a LCI unit process. A process has metadata such as a name, project and comment. It also has flows and can be linked to input or calculated parameters that can respectively be added with the methods :meth:`~eldam.core.lci_data.Process.add_parameter`, :meth:`~eldam.core.lci_data.Process.add_parameters`,  :meth:`~eldam.core.lci_data.Process.add_flow` and :meth:`~eldam.core.lci_data.Process.add_flows`.

An abstract class is defined for both :class:`~eldam.core.lci_data.Flow` and :class:`~eldam.core.lci_data.Parameter`. These classes are then inherited by classes specific to each flow or parameter types such as :class:`~eldam.core.lci_data.TechnosphereFlow` for example. The class :class:`~eldam.core.lci_data.FlowFactory` can be used for easier creation of a flow independently of its type.

Properties :meth:`~eldam.core.lci_data.Process.input_parameters`, :meth:`~eldam.core.lci_data.Process.calculated_parameters`, :meth:`~eldam.core.lci_data.Process.product_flows`, :meth:`~eldam.core.lci_data.Process.technosphere_flows` and :meth:`~eldam.core.lci_data.Process.biosphere_flows` allow direct access to a list of parameters/flows of each type linked to the process. The property :meth:`~eldam.core.lci_data.Process.product` returns the product flow if there is one and only one, :code:`None` else.

Due to SimaPro data model, if a process has a waste treatment product flow, it cannot have other product flows (classic products or waste treatment products). The property :meth:`~eldam.core.lci_data.Process.product_type` returns the type of the product and :code:`None` if the Process has no product. It is used to raise a :exc:`~eldam.utils.exceptions.ProductTypeError` if a product flow is added to the process breaking the rule explained above.

>>> from eldam.core.lci_data import Process
>>>
>>> process = Process()
>>> process.add_flows(flow1, flow1)
>>> process.add_parameters(input_parameter1, input_parameter2, calculated_parameter)
>>> process.flows
[flow1, flow2]
>>> process.input_parameters
[input_parameter1, input_parameter2]
>>> process.calculated_parameters
[calculated_parameter]

The method :meth:`~eldam.core.lci_data.Process.remove_unused_parameters` removes parameters that are not used in the formula of a flow or a calculated parameter related to the process. The method :meth:`~eldam.core.lci_data.Process.check_for_missing_parameters` will raise an :exc:`~eldam.utils.exceptions.MissingParameterError` if the formula of a flow or a calculated parameter related to the process uses a parameter that is not present in :attr:`~eldam.core.lci_data.Process.parameters`.

Flow classes
------------

The :class:`~eldam.core.lci_data.Flow` class is used for modelling a biosphere or technosphere flow linked to a Process. It is a parent class for :class:`~eldam.core.lci_data.ProductFlow`, :class:`~eldam.core.lci_data.TechnosphereFlow` and :class:`~eldam.core.lci_data.BiosphereFlow`.

The amount parameter of the class constructor can be given a float or a string. Float values will be interpreted as amount values, strings will be interpreted as amount values if parseable in a float (dot or comma are both accepted as decimal separators), and as a formula if not. The :attr:`~eldam.core.lci_data.Flow.amount_type` attribute tells if the amount has been recognized as amount value or formula.

>>> from eldam.core.lci_data import Flow
>>>
>>> flow1 = Flow(name='name', type='type', unit='unit', amount='6')
>>> flow2 = Flow(name='name', type='type', unit='unit', amount='6.5')
>>> flow3 = Flow(name='name', type='type', unit='unit', amount='6,5')
>>> flow4 = Flow(name='name', type='type', unit='unit', amount=6.5)
>>> flow5 = Flow(name='name', type='type', unit='unit', amount='A+B')
>>> flow1.amount, flow1.amount_type
6.0, 'Value'
>>> flow2.amount, flow2.amount_type
6.5, 'Value'
>>> flow3.amount, flow3.amount_type
6.5, 'Value'
>>> flow4.amount, flow4.amount_type
6.5, 'Value'
>>> flow5.amount, flow5.amount_type
'A+B', 'Formula'

For :class:`~eldam.core.lci_data.TechnosphereFlow` and :class:`~eldam.core.lci_data.BiosphereFlow`, the method :meth:`~eldam.core.lci_data.BiosphereFlow.missing_quality_data`, returns the quality data that are missing according to :ref:`ELSA's Quality Management System <Characterizing datasets quality>`.

Parameters
----------
:class:`~eldam.core.lci_data.InputParameter` and :class:`~eldam.core.lci_data.CalculatedParameter` represents parameters used in formulas. Input parameters have a value where calculated parameters have a formula (using values or others parameters). As :class:`~eldam.core.lci_data.Flow`, :class:`~eldam.core.lci_data.InputParameter` will convert strings into floats and accepts dots and commas as decimal separators. Both classes inherits from :class:`~eldam.core.lci_data.Parameter`.

Common methods
--------------
Equality and difference operators have been implemented for classes :class:`~eldam.core.lci_data.Process`, :class:`~eldam.core.lci_data.Flow`, :class:`~eldam.core.lci_data.InputParameter` and :class:`~eldam.core.lci_data.CalculatedParameter`. They evaluate equality/difference of each attributes of the objects and related flows or parameters (for processes only).

All LCI data classes inherit the following methods from the :class:`~eldam.core.lci_data.InventoryData` class :
    -   :meth:`~eldam.core.lci_data.Flow.json_data` : As some data used in the Elda format doesn't have their equivalent in SimaPro, they are stored encoded in the comment. This property returns these data encoded as a json string.
    -   :meth:`~eldam.core.lci_data.TechnosphereFlow.encoded_comment` : Returns an encoded comment containing the item's comment, and json encoded data. For :class:`~eldam.core.lci_data.TechnosphereFlow` and :class:`~eldam.core.lci_data.BiosphereFlow`, the returned encoded comment also contains quality data formatted according to ELSA data quality guidelines (`*Biard, Y. et al., 2015. Developing a quality management system for LCA datasets : the case of the LCA-CIRAD platform for tropical products. In LCM 2015.* <https://agritrop.cirad.fr/577699/>`_)
    -   :meth:`~eldam.core.lci_data.TechnosphereFlow.simapro_csv_format` : Returns a string formatted for insertion in a SimaPro csv import file.

:meth:`~eldam.core.lci_data.TechnosphereFlow.encoded_comment` and :meth:`~eldam.core.lci_data.TechnosphereFlow.simapro_csv_format` have an argument named `elda_only_data` that specifies if data used by ELDAM but not by SimaPro should be embedded in the encoded comment.
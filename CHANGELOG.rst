Changelog
=========

V1.0
----

- Modified
    - Fixed a bug on SimaPro units validation in the Elda files

V0.21
-----

- Added
    - Added support for formulas in allocation percentages

V0.20
-----

- Added
    - Added the possibility to choose which metadata to copy
    - Added protection on Elda cells that should not be modified by users
    - Added a control on the unit field to ensure that the unit exists in SimaPro
    - Added "Category type" field for processes. Category type is the top level of SimaPro's processes hierarchy.

- Modified
    - Complete interface redesign and improvements

0.19
----

- Added
    - Added the possibility to generate Elda indexes from a directory containing multiple Eldas
    - Added a field called "Inventory review state" to be able to set a review state for the inventory consistency itself
    - Eldam version check now displays last changelog in case of new version
    - Added European union and Occitanie logo on the About window

- Modified
    - GUI improvements
    - Changed quality data color codes for more consistency

0.18
----

- Modified
    - Made it possible to use semicolon in text fields
    - Fixed a bug that made ELDAM crash when opening a SimaPro .xlsx export opened and saved with Excel

0.17
----

- Added
    - Warnings before saving an Elda containing errors

- Modified
    - Improvements on version information management
    - Changed review data are not highlighted anymore, only non-review data
    - Bugfixes

0.16
----
- Added
    - Added possibility to copy metadata of an Elda to one or more Eldas
    - Added a counter when reading Eldas
    - Now possible to work with waste treatment processes
    - Waste type for product flows

V0.15
-----
- Modified
    - Minor improvements of the GUI
    - ELDAM now reloads after a crash
    - Bugfixes

V0.14
-----
- Added
    - Protected the "Status" sheet from user modification
    - ELDAs now always open on status sheet

- Modified
    - Several important bugfixes

V0.13
-----
- Added
    - Added an "About" button on main screen

V0.12
-----
- Added
    - Added button to update ELDA templates
    - Possibility to read SimaPro exports made with "Convert expressions to constants" checked

- Modified
    - ELDAs now open on status sheet
    - Improved reading of quality data from SimaPro comments

V0.11
-----
- Added
    - Added PRODUCT to the file naming pattern
    - Allocation type is mandatory if more than one flow has an allocation percentage
    - ELDAM will now automatically update ELDAs to the current ELDA template if they are based on an obsolete ELDA template

- Removed
    - Made review of empty data necessary again

V0.10
-----
- Added
    - If the user never activated the access to VBA project in Excel settings, he gets a more detailed error message than the Excel default VBA bug.
    - Improvements on the formatting of the review data
    - The version's review state is now calculated automatically


V0.9
----
- Added
    - Added informations and links about ELDAM on Eldas first sheet.
    - Data control on some fields of the Elda that must be numeric or formulas
    - Added conditional formatting for uncertainty fields
    - Fields changed from previous versions are highlighted in the Elda
    - Check if version running is the latest available

- Modified
    - Changed "mean" by "stdev" for uncertainty data

V0.8
----
- Added
    - Defined names are automatically created on parameter creation

- Modified
    - Calculated cells are greyed for it to be more obvious that they should not be modified manually


V0.7
----
- Added
    - Added support for Excel dynamic links as formulas

V0.6
----
- Modified
    - Changed logo
    - Added a background image on main window

- Added
    - Added a link to the wiki on the error message
    - Added a button to create an empty ELDA from main screen
    - Now possible to choose to export a process in a SimaPro .csv with only data handeled by SimaPro (without other data embedded in the comments)

V0.5
----
- Modified
    - Now possible to have infinite number of input or calculated parameters. A new block appears when the previous is full
    - Now possible to add up to 150 flows
    - ELDAM goes back to main screen instead of quitting. The only way to exit ELDAM is closing the window or using the Exit button on main screen

- Added
    - Added Agribalyse, Agri-footprint and "Other database" to the database list
    - Input and calculated parameters now appears in the process detailed view

V0.4
----
- Modified
    - Improved the way errors are displayed to the user
    - Improved handling of formulas in the Elda
    - Now possible to extract quality codes from SimaPro comments even if no quality comment have been entered

- Added
    - Added ELDAM version number to the main window title
    - Added possibility to use parameters names that do not respect Excel's defined names rules (ex: NO2)

- Fixed
    - Fixed a bug crashing ELDAM when trying to compare a SimaPro export to an existing Elda

V0.3
----
- Modified
    - Made "." the default decimal separator

- Added
    - Possibility to read exports from SimaPro's edition window
    - Added NOW to saving name pattern
    - Added possibility to rename the exported file in case of conflict

V0.2
----
- Modified
    - Changed "Need of corrections" by "Major corrections" in review status
    - Changed Elda color scheme

- Added
    - Colored message in GUI
    - SimaPro and Excel icons in GUI

V0.1
----
- Added
    - Convertion from SimaPro .xlsx export file to Elda
    - Conversion from Elda to .csv
    - Reading multiple processes from one .xlsx
    - Exporting multiple processes in one .csv
    - Possibility to update an existing Elda
    - Possibility to update an Elda's last version
    - Possibility to add major/minor version to an Elda
    - Database/Project parameter handling
    - Formula calculation and transcription within the Elda
    - Support for review and quality data
    - GUI with different use cases (single/multiple files conversion, file overwrite, last version update, minor/major version adding)

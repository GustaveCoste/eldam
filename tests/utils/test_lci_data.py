from eldam.utils.lci_data import *
from eldam.core.lci_data import *


def test_compare_input_parameters():
    input_parameter_1 = InputParameter(name='name1', value=5, level='Process')
    input_parameter_2 = InputParameter(name='name2', value=4, level='Process', comment='Comment')

    different = compare_parameters(input_parameter_1, input_parameter_2)
    same = compare_parameters(input_parameter_1, input_parameter_1)

    assert different == {'name': ('name1', 'name2'), 'value': (5, 4), 'comment': (None, 'Comment'),
                         'value_or_formula': (5, 4)}
    assert same == {}


def test_compare_calculated_parameters():
    calculated_parameter1 = CalculatedParameter(name='name1', formula='A+B', level='Process')
    calculated_parameter2 = CalculatedParameter(name='name2', formula='B+A', level='Process', comment='Comment')

    different = compare_parameters(calculated_parameter1, calculated_parameter2)
    same = compare_parameters(calculated_parameter1, calculated_parameter1)

    assert different == {'name': ('name1', 'name2'), 'formula': ('A+B', 'B+A'), 'comment': (None, 'Comment'),
                         'value_or_formula': ('A+B', 'B+A')}
    assert same == {}


def test_compare_flows():
    flow1 = ProductFlow(name='name1', type='Output/Technosphere/Product', unit='unit', amount=6)
    flow2 = ProductFlow(name='name2', type='Output/Technosphere/Product', unit='unit', amount='A+B')

    different = compare_flows(flow1, flow2)
    same = compare_flows(flow1, flow1)

    assert different == {'name': ('name1', 'name2'), 'amount': (6, 'A+B'), 'amount_type': ('Value', 'Formula')}
    assert same == {}


class TestCompareProcesses:
    """ Tests on compare_processes() function """

    def setup_method(self):
        self.process1 = Process(name='process', author='author', contact='contact')
        self.process2 = Process(name='PROCESS', author='AUTHOR', contact='contact',
                                metadata_review={'author': {'comment': 'comment on the author'}})

        self.flow1 = ProductFlow(name='flow', type='Output/Technosphere/Product', unit='unit', amount='A+B+6.5')
        self.flow2 = ProductFlow(name='FLOW', type='Output/Technosphere/Product', unit='unit', amount='A+B+6.5')
        self.flow3 = ProductFlow(name='FLOW', type='Output/Technosphere/Product', unit='UNIT', amount='A+B+6.5')

        self.input_parameter1 = InputParameter(name='input_parameter', comment='comment', value='8.6', level='Process')
        self.input_parameter2 = InputParameter(name='INPUT_PARAMETER', comment='comment', value='8.6', level='Process')

        self.calculated_parameter1 = CalculatedParameter(name='CALCULATED_PARAMETER', comment='comment',
                                                         formula='AAA+BBB', level='Process')
        self.calculated_parameter2 = CalculatedParameter(name='CALCULATED_PARAMETER', comment='COMMENT',
                                                         formula='AA', level='Process')

    def teardown_method(self):
        del self.process1
        del self.process2

    def test_equality(self):
        """ Asserts the function returns an empty dict when comparing two identical processes """

        self.process1.add_flows(self.flow1)
        self.process1.add_parameters(self.input_parameter1, self.calculated_parameter1)

        assert compare_processes(self.process1, self.process1) == {}

    def test_process_metadata(self):
        """ Tests the return of comparison of metadata only """

        assert compare_processes(self.process1, self.process2) == {'name': {'value': ('process', 'PROCESS')},
                                                                   'author': {'value': ('author', 'AUTHOR'),
                                                                              'comment': (
                                                                                  None, 'comment on the author')}}

    def test_flow_not_in_first_process(self):
        """ Test the case when a flow is only in the second process """

        self.process2.add_flow(self.flow1)

        assert compare_processes(self.process1, self.process2) == {'name': {'value': ('process', 'PROCESS')},
                                                                   'author': {'value': ('author', 'AUTHOR'),
                                                                              'comment': (
                                                                                  None, 'comment on the author')},
                                                                   'flows': [{'name': (None, 'flow'),
                                                                              'category': (None, 'ELDAM'),
                                                                              'type': (
                                                                                  None, 'Output/Technosphere/Product'),
                                                                              'unit': (None, 'unit'),
                                                                              'amount': (None, 'A+B+6.5'),
                                                                              'review_state': (None, 0),
                                                                              'amount_type': (None, 'Formula')}]}

    def test_flow_not_in_second_process(self):
        """ Test the case when a flow is only in the first process """

        self.process1.add_flow(self.flow1)

        assert compare_processes(self.process1, self.process2) == {'name': {'value': ('process', 'PROCESS')},
                                                                   'author': {'comment': (
                                                                       None, 'comment on the author'),
                                                                       'value': ('author', 'AUTHOR')},
                                                                   'flows': [{'amount': ('A+B+6.5', None),
                                                                              'amount_type': ('Formula', None),
                                                                              'category': ('ELDAM', None),
                                                                              'name': ('flow', None),
                                                                              'review_state': (0, None),
                                                                              'type': (
                                                                              'Output/Technosphere/Product', None),
                                                                              'unit': ('unit', None)}]
                                                                   }

    def test_flow_difference(self):
        """ Test the case when flows with the same name in both processes have different values """

        self.process1.add_flow(self.flow2)
        self.process2.add_flow(self.flow3)

        assert compare_processes(self.process1, self.process2) == {'name': {'value': ('process', 'PROCESS')},
                                                                   'author': {'value': ('author', 'AUTHOR'),
                                                                              'comment': (
                                                                                  None, 'comment on the author')},
                                                                   'flows': [{'unit': ('unit', 'UNIT'),
                                                                              'name': ('FLOW', 'FLOW')}]}

    def test_parameter_not_in_first_process(self):
        """ Test the case when a parameter is only in the second process """

        self.process2.add_parameter(self.input_parameter1)

        assert compare_processes(self.process1, self.process2) == {'name': {'value': ('process', 'PROCESS')},
                                                                   'author': {'value': ('author', 'AUTHOR'),
                                                                              'comment': (
                                                                                  None, 'comment on the author')},
                                                                   'parameters': [{'name': (None, 'input_parameter'),
                                                                                   'comment': (None, 'comment'),
                                                                                   'review_state': (None, 0),
                                                                                   'level': (None, 'Process'),
                                                                                   'type': (None, 'Input parameter'),
                                                                                   'value': (None, 8.6),
                                                                                   'value_or_formula': (None, 8.6)}]}

    def test_parameter_not_in_second_process(self):
        """ Test the case when a parameter is only in the first process """

        self.process1.add_parameter(self.input_parameter1)

        assert compare_processes(self.process1, self.process2) == {'name': {'value': ('process', 'PROCESS')},
                                                                   'author': {'value': ('author', 'AUTHOR'),
                                                                              'comment': (
                                                                                  None, 'comment on the author')},
                                                                   'parameters': [{'name': ('input_parameter', None),
                                                                                   'comment': ('comment', None),
                                                                                   'review_state': (0, None),
                                                                                   'level': ('Process', None),
                                                                                   'type': ('Input parameter', None),
                                                                                   'value': (8.6, None),
                                                                                   'value_or_formula': (8.6, None)}]}

    def test_parameter_difference(self):
        """ Test the case when parameters with the same name in both processes have different values """

        self.process1.add_parameter(self.calculated_parameter1)
        self.process2.add_parameter(self.calculated_parameter2)

        assert compare_processes(self.process1, self.process2) == {'name': {'value': ('process', 'PROCESS')},
                                                                   'author': {'value': ('author', 'AUTHOR'),
                                                                              'comment': (
                                                                                  None, 'comment on the author')},
                                                                   'parameters': [{'comment': ('comment', 'COMMENT'),
                                                                                   'formula': ('AAA+BBB', 'AA'),
                                                                                   'value_or_formula': (
                                                                                       'AAA+BBB', 'AA'),
                                                                                   'name': (
                                                                                       'CALCULATED_PARAMETER',
                                                                                       'CALCULATED_PARAMETER')}]}

import openpyxl

from tests.testing_parameters import *
from eldam.utils.xls import clean_row, compare_workbook


class TestCompareWorkbook:

    def setup_class(self):
        self.workbook1 = openpyxl.load_workbook(ELDA_A)

    def setup_method(self):
        self.workbook2 = openpyxl.load_workbook(ELDA_A)

    def teardown_method(self):
        del self.workbook2

    def test_equality(self):
        """ Asserts the function returns None when called whith the same workbook twice """
        assert compare_workbook(self.workbook1, self.workbook1) is None

    def test_cell_difference(self):
        """ Asserts the function recogize a cell if it's content has changed """
        self.workbook2.active['A1'].value = 'Changed value'

        assert compare_workbook(self.workbook1, self.workbook2, cells_summary=False)[0][1].value == 'Changed value'

    def test_cell_difference_summary(self):
        """ Asserts the function recogize a cell if it's content has changed (summary mode)"""
        self.workbook2.active['A1'].value = 'Changed value'

        assert compare_workbook(self.workbook1, self.workbook2, cells_summary=True)[0][1] == 'A1: Changed value'

    def test_cell_inexistance(self):
        """ Asserts the function recognizes when a cell exists only in one sheet """
        self.workbook2.create_sheet('New_sheet')['A1'].value = 'New value'

        compare = compare_workbook(self.workbook1, self.workbook2, cells_summary=False)
        assert compare[0][1].value == 'New value' and compare[0][0] is None

    def test_defined_name_difference(self):
        """ Assert the function recognizes a change in defined names """
        defined_name = self.workbook1.defined_names.definedName[0]
        self.workbook2.defined_names.definedName[0].attr_text = 'new_value'

        compare = compare_workbook(self.workbook1, self.workbook2)
        assert compare[0][0] == defined_name and compare[0][1].attr_text == 'new_value'

    def test_defined_name_inexistance(self):
        """ Assert the function recognizes a missing defined names """
        defined_name = self.workbook2.defined_names.definedName[0]
        self.workbook2.defined_names.definedName.remove(defined_name)

        compare = compare_workbook(self.workbook1, self.workbook2)
        assert compare[0][0] == defined_name and compare[0][1] is None


def test_clean_row():
    workbook = openpyxl.load_workbook(XLSX_A)

    for row in workbook.active.rows:
        cleaned_row = clean_row(row)

        if len(cleaned_row) > 0:
            assert cleaned_row[-1].value != '' and type(cleaned_row) == list

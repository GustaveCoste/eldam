"""
Parameters used for testing ELDAM
"""

import os

DATA_FOLDER = os.path.join(os.path.dirname(__file__), 'data')
XLSX_A = os.path.join(DATA_FOLDER, 'xlsx_a.XLSX')
XLSX_A_WITH_EXCEL_LINK = os.path.join(DATA_FOLDER, 'xlsx_a_with_excel_link.XLSX')
XLSX_A_EXPORTED_FROM_EDITION_WINDOW = os.path.join(DATA_FOLDER, 'xlsx_a_exported_from_edition_window.XLSX')
XLSX_A_CONSTANTS = os.path.join(DATA_FOLDER, 'xlsx_a_convert_expressions_to_constants.XLSX')
XLSX_B = os.path.join(DATA_FOLDER, 'xlsx_b.XLSX')
XLSX_D = os.path.join(DATA_FOLDER, 'xlsx_d.XLSX')
XLSX_E = os.path.join(DATA_FOLDER, 'xlsx_e.XLSX')
XLSX_A_B = os.path.join(DATA_FOLDER, 'xlsx_a_b.XLSX')
CSV_A = os.path.join(DATA_FOLDER, 'csv_a.csv')
CSV_B = os.path.join(DATA_FOLDER, 'csv_b.csv')
CSV_B_WITHOUT_ELDA_ONLY_DATA = os.path.join(DATA_FOLDER, 'csv_b_without_elda_only_data.csv')
CSV_C = os.path.join(DATA_FOLDER, 'csv_c.csv')
CSV_A_B = os.path.join(DATA_FOLDER, 'csv_a_b.csv')
CSV_D = os.path.join(DATA_FOLDER, 'csv_d.csv')
CSV_E = os.path.join(DATA_FOLDER, 'csv_e.csv')
NOT_AN_ELDA = os.path.join(DATA_FOLDER, 'not_an_elda.xlsm')
ELDA_A = os.path.join(DATA_FOLDER, 'elda_a.xlsm')
ELDA_A_WITH_OLD_TEMPLATE = os.path.join(DATA_FOLDER, 'elda_a_with_old_template.xlsm')
ELDA_A_WITH_EXCEL_LINK = os.path.join(DATA_FOLDER, 'elda_a_with_excel_link.xlsm')
ELDA_A_WITH_PROCESS_B_METADATA = os.path.join(DATA_FOLDER, 'elda_a_with_process_b_metadata.xlsm')
ELDA_B = os.path.join(DATA_FOLDER, 'elda_b.xlsm')
ELDA_R = os.path.join(DATA_FOLDER, 'elda_r.xlsm')
ELDA_C_MINOR = os.path.join(DATA_FOLDER, 'elda_c_minor.xlsm')
ELDA_C_MAJOR = os.path.join(DATA_FOLDER, 'elda_c_major.xlsm')
ELDA_D = os.path.join(DATA_FOLDER, 'elda_d.xlsm')
ELDA_E = os.path.join(DATA_FOLDER, 'elda_e.xlsm')

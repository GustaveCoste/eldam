import os
from datetime import datetime

import openpyxl

from eldam.core.parameters import EldaTemplateParameters
from eldam.core.elda import Elda
from eldam.utils.xls import compare_workbook
from tests.testing_parameters import DATA_FOLDER, ELDA_A, ELDA_A_WITH_OLD_TEMPLATE


class TestUpdateElda:
    def setup_class(self):
        """ Creates and saves a elda file from the simapro export """
        self.temporary_file = os.path.join(DATA_FOLDER, 'temporary_file.xlsm')

    def teardown_class(self):
        """ Deletes the elda file """
        os.remove(self.temporary_file)

    def test_update_elda(self, process_a, process_b):
        """ Asserts that when opening an ELDA based on an obsolete template,
        Elda.__init__() will update it to the current ELDA template"""

        elda = Elda(ELDA_A_WITH_OLD_TEMPLATE)
        elda.save(self.temporary_file)

        reference_workbook = openpyxl.load_workbook(ELDA_A)
        output_workbook = openpyxl.load_workbook(self.temporary_file)

        differing_cells = compare_workbook(output_workbook, reference_workbook)

        assert (differing_cells is None) or ((len(differing_cells) == 1
                                              and differing_cells[0][0] == EldaTemplateParameters().VERSION_DATE_CELL +
                                              f': {datetime.now().strftime("%d/%m/%y")}'))

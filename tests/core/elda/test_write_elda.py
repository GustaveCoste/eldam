import openpyxl

from datetime import datetime

from eldam.core.parameters import *
from eldam.core.elda import Elda
from eldam.utils.xls import compare_workbook

from tests.testing_parameters import ELDA_A, ELDA_B, ELDA_R, ELDA_C_MINOR, ELDA_C_MAJOR, ELDA_A_WITH_PROCESS_B_METADATA


def test_elda_from_scratch(process_a):
    """
    Asserts the elda output of a simple conversion of the process A corresponds to the expected result

    The only difference expected between the two workbooks is the review version date cell, which is
    automatically set to the date of the test.
    """
    elda = Elda()
    process_a.remove_unused_parameters()
    elda.add_version(process_a)

    reference_workbook = openpyxl.load_workbook(ELDA_A)

    differing_cells = compare_workbook(elda.workbook, reference_workbook)

    assert differing_cells is None or (
            len(differing_cells) == 1 and differing_cells[0][0] == EldaTemplateParameters().VERSION_DATE_CELL +
            f': {datetime.now().strftime("%d/%m/%y")}')


def test_elda_from_scratch_with_review_data(process_b):
    """ Same test as above but with review and other elda handled data """
    elda = Elda()
    process_b.remove_unused_parameters()
    elda.add_version(process_b)

    reference_workbook = openpyxl.load_workbook(ELDA_B)

    differing_cells = compare_workbook(elda.workbook, reference_workbook)

    assert differing_cells is None or (
            len(differing_cells) == 1 and differing_cells[0][0] == EldaTemplateParameters().VERSION_DATE_CELL +
            f': {datetime.now().strftime("%d/%m/%y")}')


def test_add_minor_version(process_c):
    elda = Elda(ELDA_R)
    elda.add_version(process=process_c, major_version=False)

    reference_workbook = openpyxl.load_workbook(ELDA_C_MINOR)

    differing_cells = compare_workbook(elda.workbook, reference_workbook)

    assert differing_cells is None or (
            len(differing_cells) == 1 and differing_cells[0][0] == EldaTemplateParameters().VERSION_DATE_CELL +
            f': {datetime.now().strftime("%d/%m/%y")}')


def test_add_major_version(process_c):
    elda = Elda(ELDA_R)
    elda.add_version(process=process_c, major_version=True)

    reference_workbook = openpyxl.load_workbook(ELDA_C_MAJOR)

    differing_cells = compare_workbook(elda.workbook, reference_workbook)

    assert differing_cells is None or (
            len(differing_cells) == 1 and differing_cells[0][0] == EldaTemplateParameters().VERSION_DATE_CELL +
            f': {datetime.now().strftime("%d/%m/%y")}')


def test_update_last_version(process_a, process_c):
    elda = Elda(ELDA_R)
    elda.add_version(process=process_a, major_version=True)
    elda.update_last_version(process=process_c)

    reference_workbook = openpyxl.load_workbook(ELDA_C_MAJOR)

    differing_cells = compare_workbook(elda.workbook, reference_workbook)

    assert differing_cells is None or (
            len(differing_cells) == 1 and differing_cells[0][0] == EldaTemplateParameters().VERSION_DATE_CELL +
            f': {datetime.now().strftime("%d/%m/%y")}')


def test_update_last_version_metadata(process_a, process_b):
    elda = Elda(ELDA_A)
    elda.update_last_version_metadata(process=process_b)

    reference_workbook = openpyxl.load_workbook(ELDA_A_WITH_PROCESS_B_METADATA)

    differing_cells = compare_workbook(elda.workbook, reference_workbook)

    assert differing_cells is None or (
            len(differing_cells) == 1 and differing_cells[0][0] == EldaTemplateParameters().VERSION_DATE_CELL +
            f': {datetime.now().strftime("%d/%m/%y")}')

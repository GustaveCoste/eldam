import pytest

from eldam.core.elda import Elda

from tests.testing_parameters import ELDA_A, ELDA_B, ELDA_C_MAJOR, ELDA_D, NOT_AN_ELDA, DATA_FOLDER

from eldam.utils.lci_data import *
from eldam.utils.exceptions import ExcelFormulaError, NotAnEldaError


def read_elda(process, ref_elda):
    elda = Elda(ref_elda)

    process_from_file = elda.read_last_version().to_process()

    process.remove_unused_parameters()

    compare_process = compare_processes(process_from_file, process)

    # Asserts the only possible difference is in the date of the review
    assert len(compare_process) == 0 or list(compare_process.keys()) == ['date']


def test_raise_not_an_elda_error():
    with pytest.raises(NotAnEldaError):
        elda = Elda(NOT_AN_ELDA)


def test_read_elda_a(process_a):
    read_elda(process_a, ELDA_A)


def test_read_elda_b(process_b):
    read_elda(process_b, ELDA_B)


def test_read_elda_c(process_c):
    read_elda(process_c, ELDA_C_MAJOR)


def test_read_elda_d(process_d):
    read_elda(process_d, ELDA_D)


def test_read_elda_with_formula_error():
    with pytest.raises(ExcelFormulaError):
        filepath = os.path.join(DATA_FOLDER, 'temporary_file.xlsm')

        # Creating an Elda with a flow containing a wrong formula (using Excel cells addresses)
        elda = Elda(ELDA_A)
        elda.workbook['V1.0']['H36'].value = '=L12+8'
        elda.save(filepath)

        # Reading the Elda
        elda = Elda(filepath)
        os.remove(filepath)
        elda.read_last_version().to_process()

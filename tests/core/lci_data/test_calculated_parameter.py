from eldam.core.lci_data import CalculatedParameter


def test_equality():
    """ Asserts the equality operator is functional """
    parameter = CalculatedParameter(name='name', comment='comment', formula='A+B', level='Process')
    parameter2 = CalculatedParameter(name='name', comment='comment', formula='A+B', level='Process')

    assert parameter == parameter2


def test_difference():
    """ Asserts the difference operator is functional """
    parameter = CalculatedParameter(name='name', comment='comment', formula='A+B', level='Process')
    parameter2 = CalculatedParameter(name='NAME', comment='COMMENT', formula='A+C', level='Process')

    assert parameter != parameter2

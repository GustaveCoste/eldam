from eldam.core.lci_data import Flow, ProductFlow, TechnosphereFlow, BiosphereFlow


def test_decimal_amount():
    """ Asserts the string given as amount will be recognized even with a decimal separator """
    flow = ProductFlow(name='name', type='Output/Technosphere/Product', unit='unit', amount='6')
    flow2 = ProductFlow(name='name', type='Output/Technosphere/Product', unit='unit', amount='6.5')
    flow3 = ProductFlow(name='name', type='Output/Technosphere/Product', unit='unit', amount='6,5')
    flow4 = ProductFlow(name='name', type='Output/Technosphere/Product', unit='unit', amount=6.5)

    assert flow.amount == 6 and flow.amount_type == 'Value'
    assert flow2.amount == flow3.amount == flow4.amount == 6.5


def test_formula_recognition():
    """ Asserts that if a formula is given in amount, it will be recognized as a formula """
    flow = ProductFlow(name='name', type='Output/Technosphere/Product', unit='unit', amount='A+B+6.5')

    assert flow.amount_type == 'Formula'


def test_flow_equality():
    """ Asserts the equality operator is functional """
    flow = ProductFlow(name='name', type='Output/Technosphere/Product', unit='unit', amount='A+B+6.5')
    flow2 = ProductFlow(name='name', type='Output/Technosphere/Product', unit='unit', amount='A+B+6.5')

    assert flow == flow2


def test_product_flow_difference():
    flow = ProductFlow(name='name', type='Output/Technosphere/Product', unit='unit', amount='A+B+6.5', allocation=80,
                       library='lib')
    flow2 = ProductFlow(name='name', type='Output/Technosphere/Product', unit='unit', amount='A+B+6.5', allocation=70,
                        library='lib')

    assert flow != flow2


def test_technosphere_flow_difference():
    flow = TechnosphereFlow(name='name', type='Output/Technosphere/Avoided product', unit='unit', amount='A+B+6.5',
                            modification_code=1)
    flow2 = TechnosphereFlow(name='name', type='Output/Technosphere/Avoided product', unit='unit', amount='A+B+6.5',
                             modification_code=2)

    assert flow != flow2


def test_biosphere_flow_difference():
    flow = BiosphereFlow(name='name', type='Input/Nature', unit='unit', amount='A+B+6.5',
                         relevance_code='A')
    flow2 = BiosphereFlow(name='name', type='Input/Nature', unit='unit', amount='A+B+6.5',
                          relevance_code='B')

    assert flow != flow2

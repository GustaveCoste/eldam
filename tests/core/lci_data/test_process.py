import pytest

from eldam.core.lci_data import Process, ProductFlow, BiosphereFlow, TechnosphereFlow, InputParameter, \
    CalculatedParameter
from eldam.utils.exceptions import MissingParameterError, ProductTypeError


class TestProcess:

    def setup_method(self):
        self.process = Process(name='process')
        self.input_parameter = InputParameter(name='input_parameter', comment='comment', value='8.6', level='Process')
        self.calculated_parameter = CalculatedParameter(name='calculated_parameter', comment='comment',
                                                        formula='AAA+BBB+input_parameter', level='Process')

        self.product_flow = ProductFlow(name='product1', type='Output/Technosphere/Product', unit='kg',
                                        amount='A+B+6.5')

        self.biosphere_flow1 = BiosphereFlow(name='bioflow1', type='Input/Nature', unit='kg', amount=5)
        self.biosphere_flow2 = BiosphereFlow(name='bioflow2', type='Output/Emission', unit='kg', amount=5)

        self.technosphere_flow1 = TechnosphereFlow(name='techflow1', type='Output/Technosphere/Avoided product',
                                                   unit='kg', amount=5)
        self.technosphere_flow2 = TechnosphereFlow(name='techflow2', type='Input/Technosphere', unit='kg', amount=5)

        self.process.add_flows(self.product_flow, self.biosphere_flow1, self.biosphere_flow2,
                               self.technosphere_flow1, self.technosphere_flow2)
        self.process.add_parameter(self.input_parameter)
        self.process.add_parameter(self.calculated_parameter)

    def test_properties(self):
        assert self.process.flows == [self.product_flow, self.biosphere_flow1, self.biosphere_flow2,
                                      self.technosphere_flow1, self.technosphere_flow2]
        assert self.process.product_flows == [self.product_flow]
        assert self.process.product == self.product_flow
        assert self.process.biosphere_flows == [self.biosphere_flow1, self.biosphere_flow2]
        assert self.process.technosphere_flows == [self.technosphere_flow1, self.technosphere_flow2]
        assert self.process.input_parameters[0] == self.input_parameter
        assert self.process.calculated_parameters[0] == self.calculated_parameter

    def test_product_method_returns_none_if_multiple_product_flows(self):
        product_flow2 = ProductFlow(name='product2', type='Output/Technosphere/Product', unit='kg', amount=5)

        self.process.add_flow(product_flow2)

        assert self.process.product is None

    def test_formula_check(self):
        """ Asserts that check_formula() creates unknown parameters as input_parameters """

        self.process.check_formulas()

        input_parameters_names = [parameter.name for parameter in self.process.input_parameters]
        assert input_parameters_names == ['input_parameter', 'A', 'B', 'AAA', 'BBB']

    def test_equality(self):
        """ Asserts the equality operator is functional """
        process2 = Process(name='process')

        process2.add_flows(self.product_flow, self.biosphere_flow1, self.biosphere_flow2,
                           self.technosphere_flow1, self.technosphere_flow2)
        process2.add_parameter(self.input_parameter)
        process2.add_parameter(self.calculated_parameter)

        assert self.process == process2

    def test_difference(self):
        """ Asserts the difference operator is functional """

        # A process with a different name
        process2 = Process(name='PROCESS')

        process2.add_flows(self.product_flow, self.biosphere_flow1, self.biosphere_flow2,
                           self.technosphere_flow1, self.technosphere_flow2)
        process2.add_parameter(self.input_parameter)
        process2.add_parameter(self.calculated_parameter)

        # A process with the same name but different flows
        process3 = Process(name='process')

        flow3 = ProductFlow(name='name', type='Output/Technosphere/Product', unit='UNIT', amount='A+B+6.5')

        process3.add_flow(flow3)
        process2.add_parameter(self.input_parameter)
        process2.add_parameter(self.calculated_parameter)

        # A process with the same name but different input parameters
        process4 = Process(name='process')

        input_parameter4 = InputParameter(name='INPUT_PARAMETER', comment='COMMENT', value='8.6', level='Process')

        process4.add_flows(self.product_flow, self.biosphere_flow1, self.biosphere_flow2,
                           self.technosphere_flow1, self.technosphere_flow2)
        process4.add_parameter(self.input_parameter)
        process4.add_parameter(input_parameter4)
        process4.add_parameter(self.calculated_parameter)

        assert self.process != process2
        assert self.process != process3
        assert self.process != process4

    def test_add_flows(self):
        flow2 = ProductFlow(name='name', type='Output/Technosphere/Product', unit='unit2', amount='A+B+6.5')
        flow3 = ProductFlow(name='name', type='Output/Technosphere/Product', unit='unit3', amount='A+B+6.5')

        self.process.add_flows(flow2, flow3)

        assert flow2 in self.process.flows
        assert flow3 in self.process.flows

    def test_add_parameters(self):
        input_parameter2 = InputParameter(name='input_parameter2', comment='comment', value='8.6', level='Process')
        calculated_parameter2 = CalculatedParameter(name='calculated_parameter2', comment='comment',
                                                    formula='AAA+BBB', level='Process')

        self.process.add_parameters(input_parameter2, calculated_parameter2)

        assert input_parameter2 in self.process.input_parameters
        assert calculated_parameter2 in self.process.calculated_parameters

    def test_remove_unused_parameters(self):
        self.process.remove_unused_parameters()

        input_parameters_names = [parameter.name for parameter in self.process.input_parameters]
        assert 'input_parameter' not in input_parameters_names

    def test_keep_used_parameters(self):
        flow = ProductFlow(name='name', type='Output/Technosphere/Product', unit='unit', amount='input_parameter * 3')
        self.process.add_flow(flow)
        self.process.remove_unused_parameters()

        input_parameters_names = [parameter.name for parameter in self.process.input_parameters]
        assert 'input_parameter' in input_parameters_names

    def test_check_missing_param(self):
        with pytest.raises(MissingParameterError):
            self.process.parameters = []
            self.process.check_for_missing_parameters()

    def test_product_type(self):
        assert self.process.product_type == "Output/Technosphere/Product"

    def test_process_cannot_have_different_type_products(self):
        with pytest.raises(ProductTypeError):
            self.process.add_flow(ProductFlow(name='name', type='Output/Technosphere/Waste treatment product',
                                              amount=1, unit='kg'))


def test_process_cannot_have_mutiple_waste_treatment_flows():
    with pytest.raises(ProductTypeError):
        process = Process()
        process.add_flow(ProductFlow(name='name', type='Output/Technosphere/Waste treatment product',
                                     amount=1, unit='kg'))
        process.add_flow(ProductFlow(name='name2', type='Output/Technosphere/Waste treatment product',
                                     amount=1, unit='kg'))

from eldam.core.lci_data import InputParameter


def test_decimal_value():
    """ Asserts the string given as value will be recognized even with a decimal separator """
    parameter = InputParameter(name='name', comment='comment', value='8.6', level='Process')
    parameter2 = InputParameter(name='name', comment='comment', value='8,6', level='Process')

    assert parameter.value == parameter2.value == 8.6


def test_equality():
    """ Asserts the equality operator is functional """
    parameter = InputParameter(name='name', comment='comment', value='8.6', level='Process')
    parameter2 = InputParameter(name='name', comment='comment', value='8.6', level='Process')

    assert parameter == parameter2


def test_difference():
    """ Asserts the difference operator is functional """
    parameter = InputParameter(name='name', comment='comment', value='8.6', level='Process')
    parameter2 = InputParameter(name='NAME', comment='COMMENT', value='8.6', level='Process')

    assert parameter != parameter2

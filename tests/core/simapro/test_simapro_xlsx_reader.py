import pytest

from eldam.core.simapro import SimaProXlsxReader
from eldam.core.parameters import SP_TECHNOSPHERE_FIELDS
from eldam.utils.exceptions import SimaProXlsxError
from eldam.utils.lci_data import compare_processes, compare_flows, compare_parameters

from tests.testing_parameters import XLSX_A, XLSX_B, XLSX_A_B, XLSX_A_EXPORTED_FROM_EDITION_WINDOW, XLSX_A_CONSTANTS


class TestSimaProXlsxReader:

    def setup_method(self):
        self.simapro_xlsx_reader_a = SimaProXlsxReader(XLSX_A)
        self.simapro_xlsx_reader_a_exported_from_edition_window = SimaProXlsxReader(XLSX_A_EXPORTED_FROM_EDITION_WINDOW)
        self.simapro_xlsx_reader_b = SimaProXlsxReader(XLSX_B)
        self.simapro_xlsx_reader_a_b = SimaProXlsxReader(XLSX_A_B)
        self.simapro_xlsx_reader_a_constants = SimaProXlsxReader(XLSX_A_CONSTANTS)

    def test_get_data_by_category(self):
        """ Asserts the result of get_data_by_catagory() """
        process_rows = self.simapro_xlsx_reader_a.divide_rows_by_process()[0][0]
        result = self.simapro_xlsx_reader_a.get_data_by_category(rows=process_rows,
                                                                 category='Materials/fuels',
                                                                 fields=SP_TECHNOSPHERE_FIELDS)

        assert result[0]['Name'] == \
               'Bentonite quarry infrastructure {RoW}| bentonite quarry construction | APOS, S'

    def test_get_data_by_category_inexistant_category(self):
        """ Asserts that the function will raise an error if called with an inexistant category """
        with pytest.raises(SimaProXlsxError):
            process_rows = self.simapro_xlsx_reader_a.divide_rows_by_process()[0][0]
            self.simapro_xlsx_reader_a.get_data_by_category(rows=process_rows,
                                                            category='Inexistant category',
                                                            fields=SP_TECHNOSPHERE_FIELDS)

    def test_get_data_by_category_not_enough_fields(self):
        """ Asserts that the function will raise an error if called with too few fields to store the data """
        with pytest.raises(SimaProXlsxError):
            process_rows = self.simapro_xlsx_reader_a.divide_rows_by_process()[0][0]
            self.simapro_xlsx_reader_a.get_data_by_category(rows=process_rows,
                                                            category='Materials/fuels',
                                                            fields=SP_TECHNOSPHERE_FIELDS[0])

    def test_to_process_a(self, process_a):
        ref_process = process_a
        ref_process.remove_unused_parameters()

        read_process = self.simapro_xlsx_reader_a.to_processes()[0]
        read_process.remove_unused_parameters()

        ref_process.date = read_process.date = None  # removing date as it will be different

        assert len(compare_processes(read_process, ref_process)) == 0

    def test_to_process_b(self, process_b):
        ref_process = process_b
        ref_process.remove_unused_parameters()

        read_process = self.simapro_xlsx_reader_b.to_processes()[0]
        read_process.remove_unused_parameters()

        ref_process.date = read_process.date = None  # removing date as it will be different

        assert len(compare_processes(read_process, ref_process)) == 0

    def test_to_processes_a_b(self, process_a, process_b):
        ref_process_a = process_a
        ref_process_b = process_b

        ref_process_a.remove_unused_parameters()
        ref_process_b.remove_unused_parameters()

        read_process_a, read_process_b = self.simapro_xlsx_reader_a_b.to_processes()

        read_process_a.remove_unused_parameters()
        read_process_b.remove_unused_parameters()

        ref_process_a.date = read_process_a.date = ref_process_b.date = read_process_b.date = None  # removing date as it will be different

        assert len(compare_processes(read_process_a, ref_process_a)) + len(compare_processes(read_process_b, read_process_b)) == 0

    def test_to_process_a_exported_from_edition_window(self, process_a):
        ref_process = process_a
        read_process = self.simapro_xlsx_reader_a.to_processes()[0]

        # Changing the formula of the flow needing a project level parameter
        [flow for flow in ref_process.flows if flow.amount == 'project_input_parameter'][0].amount = 'A'
        [flow for flow in read_process.flows if flow.amount == 'project_input_parameter'][0].amount = 'A'

        ref_process.remove_unused_parameters()
        read_process.remove_unused_parameters()

        ref_process.date = read_process.date = None  # removing date as it will be different

        assert read_process == ref_process

    def test_to_process_a_with_expressions_converted_to_constants(self, process_a_constants):
        ref_process = process_a_constants
        read_process = self.simapro_xlsx_reader_a_constants.to_processes()[0]

        ref_process.date = read_process.date = None  # removing date as it will be different

        assert read_process == ref_process

import pytest

from eldam.core.simapro import SimaProCsvWriter
from eldam.utils.misc import compare_file
from eldam.utils.exceptions import ParameterConflictError
from tests.testing_parameters import *


class TestSimaProCsvWriter:

    def setup_method(self):
        self.filepath = os.path.join(DATA_FOLDER, 'temporary_file.csv')

    def teardown_method(self):
        os.remove(self.filepath)

    def single_process_to_csv(self, process, ref_csv, elda_only_data=True):
        process.remove_unused_parameters()
        SimaProCsvWriter(process, elda_only_data=elda_only_data).to_csv(self.filepath)

        assert compare_file(self.filepath, ref_csv) == ''

    def test_to_csv_a(self, process_a):
        self.single_process_to_csv(process_a, CSV_A)

    def test_to_csv_b(self, process_b):
        self.single_process_to_csv(process_b, CSV_B)

    def test_to_csv_b_without_elda_only_data(self, process_b):
        self.single_process_to_csv(process_b, CSV_B_WITHOUT_ELDA_ONLY_DATA, elda_only_data=False)

    def test_to_csv_c(self, process_c):
        self.single_process_to_csv(process_c, CSV_C)

    def test_parameter_conflict_error(self, process_a, process_b):
        with pytest.raises(ParameterConflictError):

            _process_b = process_b
            param = [param for param in _process_b.parameters if param.level != 'Process'][0]
            param.comment = 'New comment'

            SimaProCsvWriter(process_a, _process_b).to_csv(self.filepath)

        # For avoiding the teardown method to fail
        with open(self.filepath, 'w') as file:
            file.write('')

    def test_a_b_to_csv(self, process_a, process_b):
        process_a.remove_unused_parameters()
        process_b.remove_unused_parameters()

        try:
            SimaProCsvWriter(process_a, process_b).to_csv(self.filepath)
        except ParameterConflictError as error:
            chosen_parameters = [params[0] for params in error.args[0]]  # Simulating user choice
            SimaProCsvWriter(process_a, process_b, chosen_parameters=chosen_parameters).to_csv(self.filepath)

        assert compare_file(self.filepath, CSV_A_B) == ''
